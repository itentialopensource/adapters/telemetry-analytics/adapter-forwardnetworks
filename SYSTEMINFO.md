# Forward Networks

Vendor: Forward Networks
Homepage: https://www.forwardnetworks.com/

Product: Forward Enterprise
Product Page: https://www.forwardnetworks.com/forward-enterprise/

## Introduction
We classify Forward Networks into the Service Assurance domain as Forward Enterprise, known as Forward Networks's product, provides capabilities to manage and verify the reliability, performance and security of network services. 

"Forward Enterprise allows path analysis, end-to-end visibility, and lighting-fast troubleshooting to keep energy networks running on all cylinders" 

## Why Integrate
The Forward Networks adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Forward Enterprise. With this adapter you have the ability to perform operations such as:

- NQE
- Network Topology
- Path Search
- Devices
- Locations

## Additional Product Documentation
The [API documents for Forward Networks]()