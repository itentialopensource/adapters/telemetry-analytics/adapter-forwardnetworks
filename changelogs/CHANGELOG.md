
## 3.0.0 [02-27-2023]

* Update adapter with latest swagger

See merge request itentialopensource/adapters/telemetry-analytics/adapter-forwardnetworks!8

---

## 2.0.0 [02-24-2023]

* Update adapter with latest swagger

See merge request itentialopensource/adapters/telemetry-analytics/adapter-forwardnetworks!8

---

## 1.1.0 [05-28-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-forwardnetworks!6

---

## 1.0.0 [05-20-2021]

- New swagger provided by Forward Networks has -
  - 2 calls that have been removed those have been removed from the adapter
  - some data fields have been removed those have been removed from the output schemas
  - enumerations have changed

See merge request itentialopensource/adapters/telemetry-analytics/adapter-forwardnetworks!5

---

## 0.9.5 [04-22-2021]

- Change the upload sample files

See merge request itentialopensource/adapters/telemetry-analytics/adapter-forwardnetworks!4

---

## 0.9.4 [04-22-2021]

- Add sample upload files and new adapter-utils

See merge request itentialopensource/adapters/telemetry-analytics/adapter-forwardnetworks!3

---

## 0.9.3 [04-21-2021]

- Fixed the createSnapshotUsingPOST call and added 26 new calls

See merge request itentialopensource/adapters/telemetry-analytics/adapter-forwardnetworks!2

---

## 0.9.2 [03-05-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/telemetry-analytics/adapter-forwardnetworks!1

---

## 0.9.1 [12-23-2020]

- Initial Commit

See commit 2ee383a

---
