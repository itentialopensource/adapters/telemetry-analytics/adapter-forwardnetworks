
## 3.3.4 [10-15-2024]

* Changes made at 2024.10.14_20:43PM

See merge request itentialopensource/adapters/adapter-forwardnetworks!21

---

## 3.3.3 [08-26-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-forwardnetworks!19

---

## 3.3.2 [08-14-2024]

* Changes made at 2024.08.14_19:00PM

See merge request itentialopensource/adapters/adapter-forwardnetworks!18

---

## 3.3.1 [08-07-2024]

* Changes made at 2024.08.06_20:12PM

See merge request itentialopensource/adapters/adapter-forwardnetworks!17

---

## 3.3.0 [07-23-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-forwardnetworks!16

---

## 3.2.3 [03-27-2024]

* Changes made at 2024.03.27_13:18PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-forwardnetworks!15

---

## 3.2.2 [03-14-2024]

* Update metadata.json

See merge request itentialopensource/adapters/telemetry-analytics/adapter-forwardnetworks!14

---

## 3.2.1 [03-12-2024]

* Changes made at 2024.03.12_11:01AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-forwardnetworks!13

---

## 3.2.0 [03-06-2024]

* Updated based on new api spec, added generic query oj, additionalInfo obj to GET calls

See merge request itentialopensource/adapters/telemetry-analytics/adapter-forwardnetworks!12

---

## 3.1.2 [02-27-2024]

* Changes made at 2024.02.27_11:36AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-forwardnetworks!11

---

## 3.1.1 [01-03-2024]

* update metadata link

See merge request itentialopensource/adapters/telemetry-analytics/adapter-forwardnetworks!10

---

## 3.1.0 [01-03-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-forwardnetworks!9

---
