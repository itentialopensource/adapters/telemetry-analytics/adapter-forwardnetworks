/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-forwardnetworks',
      type: 'ForwardNetworks',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const ForwardNetworks = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] ForwardNetworks Adapter Test', () => {
  describe('ForwardNetworks Class Tests', () => {
    const a = new ForwardNetworks(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('forwardnetworks'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('forwardnetworks'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('ForwardNetworks', pronghornDotJson.export);
          assert.equal('ForwardNetworks', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-forwardnetworks', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('forwardnetworks'));
          assert.equal('ForwardNetworks', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-forwardnetworks', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-forwardnetworks', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getNetworksUsingGET - errors', () => {
      it('should have a getNetworksUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworksUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkUsingPOST - errors', () => {
      it('should have a createNetworkUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createNetworkUsingPOST(null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-createNetworkUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkUsingDELETE - errors', () => {
      it('should have a deleteNetworkUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteNetworkUsingDELETE(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteNetworkUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkUsingPATCH - errors', () => {
      it('should have a updateNetworkUsingPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkUsingPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkUsingPATCH(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-updateNetworkUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing update', (done) => {
        try {
          a.updateNetworkUsingPATCH('fakeparam', null, (data, error) => {
            try {
              const displayE = 'update is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-updateNetworkUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelCollectUsingPOST - errors', () => {
      it('should have a cancelCollectUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.cancelCollectUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.cancelCollectUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-cancelCollectUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCollectorStateUsingGET - errors', () => {
      it('should have a getCollectorStateUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getCollectorStateUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getCollectorStateUsingGET(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getCollectorStateUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#collectUsingPOST - errors', () => {
      it('should have a collectUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.collectUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.collectUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-collectUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCredentialsUsingGET - errors', () => {
      it('should have a getDeviceCredentialsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceCredentialsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getDeviceCredentialsUsingGET(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getDeviceCredentialsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDeviceCredentialUsingPOST - errors', () => {
      it('should have a createDeviceCredentialUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.createDeviceCredentialUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createDeviceCredentialUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-createDeviceCredentialUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceCredential', (done) => {
        try {
          a.createDeviceCredentialUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceCredential is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-createDeviceCredentialUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceCredentialUsingDELETE - errors', () => {
      it('should have a deleteDeviceCredentialUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDeviceCredentialUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteDeviceCredentialUsingDELETE(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteDeviceCredentialUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.deleteDeviceCredentialUsingDELETE('fakeparam', null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteDeviceCredentialUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDeviceCredentialUsingPATCH - errors', () => {
      it('should have a patchDeviceCredentialUsingPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.patchDeviceCredentialUsingPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.patchDeviceCredentialUsingPATCH(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchDeviceCredentialUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentialId', (done) => {
        try {
          a.patchDeviceCredentialUsingPATCH('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'credentialId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchDeviceCredentialUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing update', (done) => {
        try {
          a.patchDeviceCredentialUsingPATCH('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'update is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchDeviceCredentialUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceSourcesUsingGET - errors', () => {
      it('should have a getDeviceSourcesUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceSourcesUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getDeviceSourcesUsingGET(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getDeviceSourcesUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addOrUpdateDeviceSourcesUsingPOST - errors', () => {
      it('should have a addOrUpdateDeviceSourcesUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addOrUpdateDeviceSourcesUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.addOrUpdateDeviceSourcesUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addOrUpdateDeviceSourcesUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceSources', (done) => {
        try {
          a.addOrUpdateDeviceSourcesUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceSources is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addOrUpdateDeviceSourcesUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceSourceUsingPUT - errors', () => {
      it('should have a updateDeviceSourceUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.updateDeviceSourceUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateDeviceSourceUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-updateDeviceSourceUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceSourceName', (done) => {
        try {
          a.updateDeviceSourceUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'deviceSourceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-updateDeviceSourceUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceSource', (done) => {
        try {
          a.updateDeviceSourceUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceSource is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-updateDeviceSourceUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceSourceUsingDELETE - errors', () => {
      it('should have a deleteDeviceSourceUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDeviceSourceUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteDeviceSourceUsingDELETE(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteDeviceSourceUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceSourceName', (done) => {
        try {
          a.deleteDeviceSourceUsingDELETE('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceSourceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteDeviceSourceUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJumpServersUsingGET - errors', () => {
      it('should have a getJumpServersUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getJumpServersUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getJumpServersUsingGET(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getJumpServersUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createJumpServerUsingPOST - errors', () => {
      it('should have a createJumpServerUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.createJumpServerUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createJumpServerUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-createJumpServerUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jumpServer', (done) => {
        try {
          a.createJumpServerUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'jumpServer is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-createJumpServerUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJumpServerUsingDELETE - errors', () => {
      it('should have a deleteJumpServerUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteJumpServerUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteJumpServerUsingDELETE(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteJumpServerUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jumpServerId', (done) => {
        try {
          a.deleteJumpServerUsingDELETE('fakeparam', null, (data, error) => {
            try {
              const displayE = 'jumpServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteJumpServerUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#editJumpServerUsingPATCH - errors', () => {
      it('should have a editJumpServerUsingPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.editJumpServerUsingPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.editJumpServerUsingPATCH(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-editJumpServerUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jumpServerId', (done) => {
        try {
          a.editJumpServerUsingPATCH('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'jumpServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-editJumpServerUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing update', (done) => {
        try {
          a.editJumpServerUsingPATCH('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'update is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-editJumpServerUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkLayoutUsingGET - errors', () => {
      it('should have a getNetworkLayoutUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkLayoutUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkLayoutUsingGET(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getNetworkLayoutUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setNetworkLayoutUsingPOST - errors', () => {
      it('should have a setNetworkLayoutUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.setNetworkLayoutUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.setNetworkLayoutUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-setNetworkLayoutUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing layout', (done) => {
        try {
          a.setNetworkLayoutUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'layout is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-setNetworkLayoutUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNetworkSnapshotsUsingGET - errors', () => {
      it('should have a listNetworkSnapshotsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.listNetworkSnapshotsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.listNetworkSnapshotsUsingGET(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-listNetworkSnapshotsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSnapshotUsingPOST - errors', () => {
      it('should have a createSnapshotUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.createSnapshotUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing file', (done) => {
        try {
          a.createSnapshotUsingPOST(null, null, null, (data, error) => {
            try {
              const displayE = 'file is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-createSnapshotUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createSnapshotUsingPOST('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-createSnapshotUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importSnapshotWithFilesUsingPOST - errors', () => {
      it('should have a importSnapshotWithFilesUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.importSnapshotWithFilesUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing file', (done) => {
        try {
          a.importSnapshotWithFilesUsingPOST(null, null, null, (data, error) => {
            try {
              const displayE = 'file is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-importSnapshotWithFilesUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.importSnapshotWithFilesUsingPOST('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-importSnapshotWithFilesUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLatestProcessedSnapshotUsingGET - errors', () => {
      it('should have a getLatestProcessedSnapshotUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getLatestProcessedSnapshotUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getLatestProcessedSnapshotUsingGET(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getLatestProcessedSnapshotUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#zipSnapshotUsingGET - errors', () => {
      it('should have a zipSnapshotUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.zipSnapshotUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.zipSnapshotUsingGET(null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-zipSnapshotUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSnapshotUsingDELETE - errors', () => {
      it('should have a deleteSnapshotUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSnapshotUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.deleteSnapshotUsingDELETE(null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteSnapshotUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnapshotMetricsUsingGET - errors', () => {
      it('should have a getSnapshotMetricsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getSnapshotMetricsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getSnapshotMetricsUsingGET(null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getSnapshotMetricsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setNetworkTopoListUsingPUT - errors', () => {
      it('should have a setNetworkTopoListUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.setNetworkTopoListUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.setNetworkTopoListUsingPUT(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-setNetworkTopoListUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing links', (done) => {
        try {
          a.setNetworkTopoListUsingPUT('fakeparam', null, (data, error) => {
            try {
              const displayE = 'links is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-setNetworkTopoListUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyUsingGET - errors', () => {
      it('should have a getTopologyUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getTopologyUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getTopologyUsingGET(null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getTopologyUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnapshotTopoOverridesUsingGET - errors', () => {
      it('should have a getSnapshotTopoOverridesUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getSnapshotTopoOverridesUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getSnapshotTopoOverridesUsingGET(null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getSnapshotTopoOverridesUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSnapshotTopoOverridesUsingPOST - errors', () => {
      it('should have a postSnapshotTopoOverridesUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.postSnapshotTopoOverridesUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.postSnapshotTopoOverridesUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-postSnapshotTopoOverridesUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkOverridesEdit', (done) => {
        try {
          a.postSnapshotTopoOverridesUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkOverridesEdit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-postSnapshotTopoOverridesUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putSnapshotTopoOverridesUsingPUT - errors', () => {
      it('should have a putSnapshotTopoOverridesUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.putSnapshotTopoOverridesUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.putSnapshotTopoOverridesUsingPUT(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putSnapshotTopoOverridesUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkOverrides', (done) => {
        try {
          a.putSnapshotTopoOverridesUsingPUT('fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkOverrides is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putSnapshotTopoOverridesUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#runNqeQueryUsingPOST - errors', () => {
      it('should have a runNqeQueryUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.runNqeQueryUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing runRequest', (done) => {
        try {
          a.runNqeQueryUsingPOST('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'runRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-runNqeQueryUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAvailablePredefinedChecksUsingGET - errors', () => {
      it('should have a getAvailablePredefinedChecksUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getAvailablePredefinedChecksUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getChecksUsingGET - errors', () => {
      it('should have a getChecksUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getChecksUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getChecksUsingGET(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getChecksUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getChecksUsingGET('fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getChecksUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addCheckUsingPOST - errors', () => {
      it('should have a addCheckUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addCheckUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.addCheckUsingPOST(null, null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addCheckUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing check', (done) => {
        try {
          a.addCheckUsingPOST('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'check is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addCheckUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateChecksUsingDELETE - errors', () => {
      it('should have a deactivateChecksUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deactivateChecksUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.deactivateChecksUsingDELETE(null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deactivateChecksUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSingleCheckUsingGET - errors', () => {
      it('should have a getSingleCheckUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getSingleCheckUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getSingleCheckUsingGET(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getSingleCheckUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing checkId', (done) => {
        try {
          a.getSingleCheckUsingGET('fakeparam', null, (data, error) => {
            try {
              const displayE = 'checkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getSingleCheckUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateCheckUsingDELETE - errors', () => {
      it('should have a deactivateCheckUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deactivateCheckUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.deactivateCheckUsingDELETE(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deactivateCheckUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing checkId', (done) => {
        try {
          a.deactivateCheckUsingDELETE('fakeparam', null, (data, error) => {
            try {
              const displayE = 'checkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deactivateCheckUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllAliasesUsingGET - errors', () => {
      it('should have a getAllAliasesUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getAllAliasesUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getAllAliasesUsingGET(null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getAllAliasesUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSingleAliasUsingGET - errors', () => {
      it('should have a getSingleAliasUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getSingleAliasUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getSingleAliasUsingGET(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getSingleAliasUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.getSingleAliasUsingGET('fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getSingleAliasUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSnapshotAliasUsingPUT - errors', () => {
      it('should have a createSnapshotAliasUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.createSnapshotAliasUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.createSnapshotAliasUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-createSnapshotAliasUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createSnapshotAliasUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-createSnapshotAliasUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing aliasBuilder', (done) => {
        try {
          a.createSnapshotAliasUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'aliasBuilder is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-createSnapshotAliasUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateAliasUsingDELETE - errors', () => {
      it('should have a deactivateAliasUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deactivateAliasUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.deactivateAliasUsingDELETE(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deactivateAliasUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.deactivateAliasUsingDELETE('fakeparam', null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deactivateAliasUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceFilesUsingGET - errors', () => {
      it('should have a getDeviceFilesUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceFilesUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getDeviceFilesUsingGET(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getDeviceFilesUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.getDeviceFilesUsingGET('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getDeviceFilesUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceFileContentUsingGET - errors', () => {
      it('should have a getDeviceFileContentUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceFileContentUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getDeviceFileContentUsingGET(null, null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getDeviceFileContentUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.getDeviceFileContentUsingGET('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getDeviceFileContentUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileName', (done) => {
        try {
          a.getDeviceFileContentUsingGET('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'fileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getDeviceFileContentUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL2VpnsUsingGET - errors', () => {
      it('should have a getL2VpnsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getL2VpnsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getL2VpnsUsingGET(null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL2VpnsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putL2VpnsUsingPUT - errors', () => {
      it('should have a putL2VpnsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.putL2VpnsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.putL2VpnsUsingPUT(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putL2VpnsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l2VpnList', (done) => {
        try {
          a.putL2VpnsUsingPUT('fakeparam', null, (data, error) => {
            try {
              const displayE = 'l2VpnList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putL2VpnsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL2VpnUsingGET - errors', () => {
      it('should have a getL2VpnUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getL2VpnUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getL2VpnUsingGET(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL2VpnUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l2VpnName', (done) => {
        try {
          a.getL2VpnUsingGET('fakeparam', null, (data, error) => {
            try {
              const displayE = 'l2VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL2VpnUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putL2VpnUsingPUT - errors', () => {
      it('should have a putL2VpnUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.putL2VpnUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.putL2VpnUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putL2VpnUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l2VpnName', (done) => {
        try {
          a.putL2VpnUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'l2VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putL2VpnUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l2Vpn', (done) => {
        try {
          a.putL2VpnUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'l2Vpn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putL2VpnUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteL2VpnUsingDELETE - errors', () => {
      it('should have a deleteL2VpnUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteL2VpnUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.deleteL2VpnUsingDELETE(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL2VpnUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l2VpnName', (done) => {
        try {
          a.deleteL2VpnUsingDELETE('fakeparam', null, (data, error) => {
            try {
              const displayE = 'l2VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL2VpnUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchL2VpnUsingPATCH - errors', () => {
      it('should have a patchL2VpnUsingPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.patchL2VpnUsingPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.patchL2VpnUsingPATCH(null, null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchL2VpnUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l2VpnName', (done) => {
        try {
          a.patchL2VpnUsingPATCH('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'l2VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchL2VpnUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing patch', (done) => {
        try {
          a.patchL2VpnUsingPATCH('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'patch is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchL2VpnUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addL2VpnConnectionUsingPOST - errors', () => {
      it('should have a addL2VpnConnectionUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addL2VpnConnectionUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.addL2VpnConnectionUsingPOST(null, null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addL2VpnConnectionUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l2VpnName', (done) => {
        try {
          a.addL2VpnConnectionUsingPOST('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'l2VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addL2VpnConnectionUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connection', (done) => {
        try {
          a.addL2VpnConnectionUsingPOST('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connection is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addL2VpnConnectionUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteL2VpnConnectionUsingDELETE - errors', () => {
      it('should have a deleteL2VpnConnectionUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteL2VpnConnectionUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.deleteL2VpnConnectionUsingDELETE(null, null, null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL2VpnConnectionUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l2VpnName', (done) => {
        try {
          a.deleteL2VpnConnectionUsingDELETE('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'l2VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL2VpnConnectionUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.deleteL2VpnConnectionUsingDELETE('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL2VpnConnectionUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portName', (done) => {
        try {
          a.deleteL2VpnConnectionUsingDELETE('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'portName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL2VpnConnectionUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL3VpnsUsingGET - errors', () => {
      it('should have a getL3VpnsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getL3VpnsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getL3VpnsUsingGET(null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL3VpnsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putL3VpnsUsingPUT - errors', () => {
      it('should have a putL3VpnsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.putL3VpnsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.putL3VpnsUsingPUT(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putL3VpnsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l3Vpns', (done) => {
        try {
          a.putL3VpnsUsingPUT('fakeparam', null, (data, error) => {
            try {
              const displayE = 'l3Vpns is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putL3VpnsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL3VpnUsingGET - errors', () => {
      it('should have a getL3VpnUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getL3VpnUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getL3VpnUsingGET(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL3VpnUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l3VpnName', (done) => {
        try {
          a.getL3VpnUsingGET('fakeparam', null, (data, error) => {
            try {
              const displayE = 'l3VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL3VpnUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putL3VpnUsingPUT - errors', () => {
      it('should have a putL3VpnUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.putL3VpnUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.putL3VpnUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putL3VpnUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l3VpnName', (done) => {
        try {
          a.putL3VpnUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'l3VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putL3VpnUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l3Vpn', (done) => {
        try {
          a.putL3VpnUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'l3Vpn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putL3VpnUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteL3VpnUsingDELETE - errors', () => {
      it('should have a deleteL3VpnUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteL3VpnUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.deleteL3VpnUsingDELETE(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL3VpnUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l3VpnName', (done) => {
        try {
          a.deleteL3VpnUsingDELETE('fakeparam', null, (data, error) => {
            try {
              const displayE = 'l3VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL3VpnUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchL3VpnUsingPATCH - errors', () => {
      it('should have a patchL3VpnUsingPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.patchL3VpnUsingPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.patchL3VpnUsingPATCH(null, null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchL3VpnUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l3VpnName', (done) => {
        try {
          a.patchL3VpnUsingPATCH('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'l3VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchL3VpnUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing patch', (done) => {
        try {
          a.patchL3VpnUsingPATCH('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'patch is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchL3VpnUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addL3VpnConnectionUsingPOST - errors', () => {
      it('should have a addL3VpnConnectionUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addL3VpnConnectionUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.addL3VpnConnectionUsingPOST(null, null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addL3VpnConnectionUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l3VpnName', (done) => {
        try {
          a.addL3VpnConnectionUsingPOST('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'l3VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addL3VpnConnectionUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connection', (done) => {
        try {
          a.addL3VpnConnectionUsingPOST('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connection is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addL3VpnConnectionUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteL3VpnEdgePortUsingDELETE - errors', () => {
      it('should have a deleteL3VpnEdgePortUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteL3VpnEdgePortUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.deleteL3VpnEdgePortUsingDELETE(null, null, null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL3VpnEdgePortUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l3VpnName', (done) => {
        try {
          a.deleteL3VpnEdgePortUsingDELETE('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'l3VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL3VpnEdgePortUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.deleteL3VpnEdgePortUsingDELETE('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL3VpnEdgePortUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portName', (done) => {
        try {
          a.deleteL3VpnEdgePortUsingDELETE('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'portName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL3VpnEdgePortUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPathsUsingGET - errors', () => {
      it('should have a getPathsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getPathsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getPathsUsingGET(null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getPathsBySnapshotUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing srcIp', (done) => {
        try {
          a.getPathsUsingGET('fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'srcIp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getPathsBySnapshotUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dstIp', (done) => {
        try {
          a.getPathsUsingGET('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'dstIp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getPathsBySnapshotUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPathsByNetworkIdUsingGET - errors', () => {
      it('should have a getPathsByNetworkIdUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getPathsByNetworkIdUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getPathsByNetworkIdUsingGET(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getPathsByNetworkIdUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing srcIp', (done) => {
        try {
          a.getPathsByNetworkIdUsingGET('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'srcIp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getPathsByNetworkIdUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dstIp', (done) => {
        try {
          a.getPathsByNetworkIdUsingGET('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'dstIp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getPathsByNetworkIdUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPathsBulkUsingPOST - errors', () => {
      it('should have a getPathsBulkUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.getPathsBulkUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getPathsBulkUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getPathsBulkUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.getPathsBulkUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getPathsBulkUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPathsBulkByNetworkIdUsingPOST - errors', () => {
      it('should have a getPathsBulkByNetworkIdUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.getPathsBulkByNetworkIdUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getPathsBulkByNetworkIdUsingPOST(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getPathsBulkByNetworkIdUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.getPathsBulkByNetworkIdUsingPOST('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getPathsBulkByNetworkIdUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTracePathsBulkSeqUsingPOST - errors', () => {
      it('should have a getTracePathsBulkSeqUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.getTracePathsBulkSeqUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getTracePathsBulkSeqUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getTracePathsBulkSeqUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.getTracePathsBulkSeqUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getTracePathsBulkSeqUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTracePathsBulkSeqByNetworkIdUsingPOST - errors', () => {
      it('should have a getTracePathsBulkSeqByNetworkIdUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.getTracePathsBulkSeqByNetworkIdUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getTracePathsBulkSeqByNetworkIdUsingPOST(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getTracePathsBulkSeqByNetworkIdUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.getTracePathsBulkSeqByNetworkIdUsingPOST('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getTracePathsBulkSeqByNetworkIdUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL7ApplicationsUsingGET - errors', () => {
      it('should have a getL7ApplicationsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getL7ApplicationsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiVersionUsingGET - errors', () => {
      it('should have a getApiVersionUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getApiVersionUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDeviceCredentialsUsingPATCH - errors', () => {
      it('should have a createDeviceCredentialsUsingPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.createDeviceCredentialsUsingPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createDeviceCredentialsUsingPATCH(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-createDeviceCredentialsUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing credentials', (done) => {
        try {
          a.createDeviceCredentialsUsingPATCH('fakeparam', null, (data, error) => {
            try {
              const displayE = 'credentials is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-createDeviceCredentialsUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceSourcesUsingDELETE - errors', () => {
      it('should have a deleteDeviceSourcesUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDeviceSourcesUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteDeviceSourcesUsingDELETE(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteDeviceSourcesUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing criteria', (done) => {
        try {
          a.deleteDeviceSourcesUsingDELETE('fakeparam', null, (data, error) => {
            try {
              const displayE = 'criteria is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteDeviceSourcesUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#customZipSnapshotUsingPOST - errors', () => {
      it('should have a customZipSnapshotUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.customZipSnapshotUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.customZipSnapshotUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-customZipSnapshotUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing params', (done) => {
        try {
          a.customZipSnapshotUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'params is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-customZipSnapshotUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateAliasesUsingDELETE - errors', () => {
      it('should have a deactivateAliasesUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deactivateAliasesUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.deactivateAliasesUsingDELETE(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deactivateAliasesUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicesUsingGET - errors', () => {
      it('should have a getDevicesUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getDevicesUsingGET(null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getDevicesUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicesByNetworkIdUsingGET - errors', () => {
      it('should have a getDevicesByNetworkIdUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesByNetworkIdUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getDevicesByNetworkIdUsingGET(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getDevicesByNetworkIdUsingGET', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOneDeviceUsingGET - errors', () => {
      it('should have a getOneDeviceUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getOneDeviceUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getOneDeviceUsingGET(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getOneDeviceUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.getOneDeviceUsingGET('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getOneDeviceUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMissingDevicesUsingGET - errors', () => {
      it('should have a getMissingDevicesUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getMissingDevicesUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getMissingDevicesUsingGET(null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getMissingDevicesUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInternetNodeUsingGET - errors', () => {
      it('should have a getInternetNodeUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getInternetNodeUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getInternetNodeUsingGET(null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getInternetNodeUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putInternetNodeUsingPUT - errors', () => {
      it('should have a putInternetNodeUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.putInternetNodeUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.putInternetNodeUsingPUT(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putInternetNodeUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing internetNode', (done) => {
        try {
          a.putInternetNodeUsingPUT('fakeparam', null, (data, error) => {
            try {
              const displayE = 'internetNode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putInternetNodeUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInternetNodeUsingDELETE - errors', () => {
      it('should have a deleteInternetNodeUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInternetNodeUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.deleteInternetNodeUsingDELETE(null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteInternetNodeUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addInternetNodeConnectionUsingPOST - errors', () => {
      it('should have a addInternetNodeConnectionUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addInternetNodeConnectionUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.addInternetNodeConnectionUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addInternetNodeConnectionUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connection', (done) => {
        try {
          a.addInternetNodeConnectionUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'connection is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addInternetNodeConnectionUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInternetNodeConnectionUsingDELETE - errors', () => {
      it('should have a deleteInternetNodeConnectionUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInternetNodeConnectionUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.deleteInternetNodeConnectionUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteInternetNodeConnectionUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.deleteInternetNodeConnectionUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteInternetNodeConnectionUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portName', (done) => {
        try {
          a.deleteInternetNodeConnectionUsingDELETE('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'portName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteInternetNodeConnectionUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntranetNodesUsingGET - errors', () => {
      it('should have a getIntranetNodesUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getIntranetNodesUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getIntranetNodesUsingGET(null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getIntranetNodesUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIntranetNodesUsingPUT - errors', () => {
      it('should have a putIntranetNodesUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.putIntranetNodesUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.putIntranetNodesUsingPUT(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putIntranetNodesUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing intranetNodeList', (done) => {
        try {
          a.putIntranetNodesUsingPUT('fakeparam', null, (data, error) => {
            try {
              const displayE = 'intranetNodeList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putIntranetNodesUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntranetNodeUsingGET - errors', () => {
      it('should have a getIntranetNodeUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getIntranetNodeUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getIntranetNodeUsingGET(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getIntranetNodeUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.getIntranetNodeUsingGET('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getIntranetNodeUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIntranetNodeUsingPUT - errors', () => {
      it('should have a putIntranetNodeUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.putIntranetNodeUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.putIntranetNodeUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putIntranetNodeUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.putIntranetNodeUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putIntranetNodeUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing intranetNode', (done) => {
        try {
          a.putIntranetNodeUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'intranetNode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putIntranetNodeUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIntranetNodeUsingDELETE - errors', () => {
      it('should have a deleteIntranetNodeUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIntranetNodeUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.deleteIntranetNodeUsingDELETE(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteIntranetNodeUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.deleteIntranetNodeUsingDELETE('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteIntranetNodeUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIntranetNodeUsingPATCH - errors', () => {
      it('should have a patchIntranetNodeUsingPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.patchIntranetNodeUsingPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.patchIntranetNodeUsingPATCH(null, null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchIntranetNodeUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.patchIntranetNodeUsingPATCH('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchIntranetNodeUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing patch', (done) => {
        try {
          a.patchIntranetNodeUsingPATCH('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'patch is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchIntranetNodeUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addIntranetNodeConnectionUsingPOST - errors', () => {
      it('should have a addIntranetNodeConnectionUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addIntranetNodeConnectionUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.addIntranetNodeConnectionUsingPOST(null, null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addIntranetNodeConnectionUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.addIntranetNodeConnectionUsingPOST('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addIntranetNodeConnectionUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connection', (done) => {
        try {
          a.addIntranetNodeConnectionUsingPOST('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connection is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addIntranetNodeConnectionUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIntranetNodeConnectionUsingDELETE - errors', () => {
      it('should have a deleteIntranetNodeConnectionUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIntranetNodeConnectionUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.deleteIntranetNodeConnectionUsingDELETE(null, null, null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteIntranetNodeConnectionUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.deleteIntranetNodeConnectionUsingDELETE('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteIntranetNodeConnectionUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.deleteIntranetNodeConnectionUsingDELETE('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteIntranetNodeConnectionUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portName', (done) => {
        try {
          a.deleteIntranetNodeConnectionUsingDELETE('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'portName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteIntranetNodeConnectionUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWanCircuitsUsingGET - errors', () => {
      it('should have a getWanCircuitsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getWanCircuitsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getWanCircuitsUsingGET(null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getWanCircuitsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWanCircuitsUsingPUT - errors', () => {
      it('should have a putWanCircuitsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.putWanCircuitsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.putWanCircuitsUsingPUT(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putWanCircuitsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wanCircuitList', (done) => {
        try {
          a.putWanCircuitsUsingPUT('fakeparam', null, (data, error) => {
            try {
              const displayE = 'wanCircuitList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putWanCircuitsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWanCircuitUsingGET - errors', () => {
      it('should have a getWanCircuitUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getWanCircuitUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getWanCircuitUsingGET(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getWanCircuitUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wanCircuitName', (done) => {
        try {
          a.getWanCircuitUsingGET('fakeparam', null, (data, error) => {
            try {
              const displayE = 'wanCircuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getWanCircuitUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWanCircuitUsingPUT - errors', () => {
      it('should have a putWanCircuitUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.putWanCircuitUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.putWanCircuitUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putWanCircuitUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wanCircuitName', (done) => {
        try {
          a.putWanCircuitUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'wanCircuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putWanCircuitUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wanCircuit', (done) => {
        try {
          a.putWanCircuitUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'wanCircuit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putWanCircuitUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWanCircuitUsingDELETE - errors', () => {
      it('should have a deleteWanCircuitUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWanCircuitUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.deleteWanCircuitUsingDELETE(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteWanCircuitUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wanCircuitName', (done) => {
        try {
          a.deleteWanCircuitUsingDELETE('fakeparam', null, (data, error) => {
            try {
              const displayE = 'wanCircuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteWanCircuitUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchWanCircuitUsingPATCH - errors', () => {
      it('should have a patchWanCircuitUsingPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.patchWanCircuitUsingPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.patchWanCircuitUsingPATCH(null, null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchWanCircuitUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wanCircuitName', (done) => {
        try {
          a.patchWanCircuitUsingPATCH('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'wanCircuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchWanCircuitUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing patch', (done) => {
        try {
          a.patchWanCircuitUsingPATCH('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'patch is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchWanCircuitUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCveIndexUsingGET - errors', () => {
      it('should have a getCveIndexUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getCveIndexUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putCveIndexUsingPUT - errors', () => {
      it('should have a putCveIndexUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.putCveIndexUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing content', (done) => {
        try {
          a.putCveIndexUsingPUT(null, null, (data, error) => {
            try {
              const displayE = 'content is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putCveIndexUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportDatabaseUsingGET - errors', () => {
      it('should have a exportDatabaseUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.exportDatabaseUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importDatabaseUsingPUT - errors', () => {
      it('should have a importDatabaseUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.importDatabaseUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createWorkspaceNetworkUsingPOST - errors', () => {
      it('should have a createWorkspaceNetworkUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.createWorkspaceNetworkUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createWorkspaceNetworkUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-createWorkspaceNetworkUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.createWorkspaceNetworkUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-createWorkspaceNetworkUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAtlasUsingGET - errors', () => {
      it('should have a getAtlasUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getAtlasUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getAtlasUsingGET(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getAtlasUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAtlasUsingPATCH - errors', () => {
      it('should have a updateAtlasUsingPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.updateAtlasUsingPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateAtlasUsingPATCH(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-updateAtlasUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing patch', (done) => {
        try {
          a.updateAtlasUsingPATCH('fakeparam', null, (data, error) => {
            try {
              const displayE = 'patch is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-updateAtlasUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLocationsUsingGET - errors', () => {
      it('should have a getLocationsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getLocationsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getLocationsUsingGET(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getLocationsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addLocationUsingPOST - errors', () => {
      it('should have a addLocationUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addLocationUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.addLocationUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addLocationUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing newLocation', (done) => {
        try {
          a.addLocationUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'newLocation is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addLocationUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchLocationsUsingPATCH - errors', () => {
      it('should have a patchLocationsUsingPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.patchLocationsUsingPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.patchLocationsUsingPATCH(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchLocationsUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing patches', (done) => {
        try {
          a.patchLocationsUsingPATCH('fakeparam', null, (data, error) => {
            try {
              const displayE = 'patches is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchLocationsUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putLocationsUsingPUT - errors', () => {
      it('should have a putLocationsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.putLocationsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.putLocationsUsingPUT(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putLocationsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing locations', (done) => {
        try {
          a.putLocationsUsingPUT('fakeparam', null, (data, error) => {
            try {
              const displayE = 'locations is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putLocationsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLocationUsingGET - errors', () => {
      it('should have a getLocationUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getLocationUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getLocationUsingGET(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getLocationUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing locationId', (done) => {
        try {
          a.getLocationUsingGET('fakeparam', null, (data, error) => {
            try {
              const displayE = 'locationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getLocationUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLocationUsingDELETE - errors', () => {
      it('should have a deleteLocationUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLocationUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteLocationUsingDELETE(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteLocationUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing locationId', (done) => {
        try {
          a.deleteLocationUsingDELETE('fakeparam', null, (data, error) => {
            try {
              const displayE = 'locationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteLocationUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchLocationUsingPATCH - errors', () => {
      it('should have a patchLocationUsingPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.patchLocationUsingPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.patchLocationUsingPATCH(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchLocationUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing locationId', (done) => {
        try {
          a.patchLocationUsingPATCH('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'locationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchLocationUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing patch', (done) => {
        try {
          a.patchLocationUsingPATCH('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'patch is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchLocationUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceClustersUsingGET - errors', () => {
      it('should have a getDeviceClustersUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceClustersUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getDeviceClustersUsingGET(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getDeviceClustersUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing locationId', (done) => {
        try {
          a.getDeviceClustersUsingGET('fakeparam', null, (data, error) => {
            try {
              const displayE = 'locationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getDeviceClustersUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addDeviceClusterUsingPOST - errors', () => {
      it('should have a addDeviceClusterUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addDeviceClusterUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.addDeviceClusterUsingPOST(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addDeviceClusterUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing locationId', (done) => {
        try {
          a.addDeviceClusterUsingPOST('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'locationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addDeviceClusterUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cluster', (done) => {
        try {
          a.addDeviceClusterUsingPOST('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'cluster is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addDeviceClusterUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putDeviceClustersUsingPUT - errors', () => {
      it('should have a putDeviceClustersUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.putDeviceClustersUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.putDeviceClustersUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putDeviceClustersUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing locationId', (done) => {
        try {
          a.putDeviceClustersUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'locationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putDeviceClustersUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusters', (done) => {
        try {
          a.putDeviceClustersUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'clusters is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putDeviceClustersUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceClusterUsingGET - errors', () => {
      it('should have a getDeviceClusterUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceClusterUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getDeviceClusterUsingGET(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getDeviceClusterUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing locationId', (done) => {
        try {
          a.getDeviceClusterUsingGET('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'locationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getDeviceClusterUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterName', (done) => {
        try {
          a.getDeviceClusterUsingGET('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'clusterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getDeviceClusterUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceClusterUsingDELETE - errors', () => {
      it('should have a deleteDeviceClusterUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDeviceClusterUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteDeviceClusterUsingDELETE(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteDeviceClusterUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing locationId', (done) => {
        try {
          a.deleteDeviceClusterUsingDELETE('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'locationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteDeviceClusterUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterName', (done) => {
        try {
          a.deleteDeviceClusterUsingDELETE('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'clusterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteDeviceClusterUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceClusterUsingPATCH - errors', () => {
      it('should have a updateDeviceClusterUsingPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.updateDeviceClusterUsingPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateDeviceClusterUsingPATCH(null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-updateDeviceClusterUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing locationId', (done) => {
        try {
          a.updateDeviceClusterUsingPATCH('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'locationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-updateDeviceClusterUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clusterName', (done) => {
        try {
          a.updateDeviceClusterUsingPATCH('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'clusterName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-updateDeviceClusterUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing update', (done) => {
        try {
          a.updateDeviceClusterUsingPATCH('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'update is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-updateDeviceClusterUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCollectionSchedulesUsingGET - errors', () => {
      it('should have a getCollectionSchedulesUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getCollectionSchedulesUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getCollectionSchedulesUsingGET(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getCollectionSchedulesUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addCollectionScheduleUsingPOST - errors', () => {
      it('should have a addCollectionScheduleUsingPOST function', (done) => {
        try {
          assert.equal(true, typeof a.addCollectionScheduleUsingPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.addCollectionScheduleUsingPOST(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addCollectionScheduleUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing schedule', (done) => {
        try {
          a.addCollectionScheduleUsingPOST('fakeparam', null, (data, error) => {
            try {
              const displayE = 'schedule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addCollectionScheduleUsingPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCollectionScheduleUsingGET - errors', () => {
      it('should have a getCollectionScheduleUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getCollectionScheduleUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getCollectionScheduleUsingGET(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getCollectionScheduleUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scheduleId', (done) => {
        try {
          a.getCollectionScheduleUsingGET('fakeparam', null, (data, error) => {
            try {
              const displayE = 'scheduleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getCollectionScheduleUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceCollectionScheduleUsingPUT - errors', () => {
      it('should have a replaceCollectionScheduleUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.replaceCollectionScheduleUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.replaceCollectionScheduleUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-replaceCollectionScheduleUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scheduleId', (done) => {
        try {
          a.replaceCollectionScheduleUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'scheduleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-replaceCollectionScheduleUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing schedule', (done) => {
        try {
          a.replaceCollectionScheduleUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'schedule is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-replaceCollectionScheduleUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCollectionScheduleUsingDELETE - errors', () => {
      it('should have a deleteCollectionScheduleUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCollectionScheduleUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteCollectionScheduleUsingDELETE(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteCollectionScheduleUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scheduleId', (done) => {
        try {
          a.deleteCollectionScheduleUsingDELETE('fakeparam', null, (data, error) => {
            try {
              const displayE = 'scheduleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteCollectionScheduleUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceSourceUsingGET - errors', () => {
      it('should have a getDeviceSourceUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceSourceUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getDeviceSourceUsingGET(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getDeviceSourceUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceSourceName', (done) => {
        try {
          a.getDeviceSourceUsingGET('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceSourceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getDeviceSourceUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDeviceSourceUsingPATCH - errors', () => {
      it('should have a patchDeviceSourceUsingPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.patchDeviceSourceUsingPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.patchDeviceSourceUsingPATCH(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchDeviceSourceUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceSourceName', (done) => {
        try {
          a.patchDeviceSourceUsingPATCH('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'deviceSourceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchDeviceSourceUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing patch', (done) => {
        try {
          a.patchDeviceSourceUsingPATCH('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'patch is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchDeviceSourceUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNqeQueriesUsingGET - errors', () => {
      it('should have a getNqeQueriesUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getNqeQueriesUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVulnerabilitiesUsingGET - errors', () => {
      it('should have a getVulnerabilitiesUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getVulnerabilitiesUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.getVulnerabilitiesUsingGET(null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getVulnerabilitiesUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEncryptorsUsingGET - errors', () => {
      it('should have a getEncryptorsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getEncryptorsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getEncryptorsUsingGET(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getEncryptorsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putEncryptorsUsingPUT - errors', () => {
      it('should have a putEncryptorsUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.putEncryptorsUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.putEncryptorsUsingPUT(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putEncryptorsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encryptorList', (done) => {
        try {
          a.putEncryptorsUsingPUT('fakeparam', null, (data, error) => {
            try {
              const displayE = 'encryptorList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putEncryptorsUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEncryptorUsingGET - errors', () => {
      it('should have a getEncryptorUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getEncryptorUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getEncryptorUsingGET(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getEncryptorUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.getEncryptorUsingGET('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getEncryptorUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putEncryptorUsingPUT - errors', () => {
      it('should have a putEncryptorUsingPUT function', (done) => {
        try {
          assert.equal(true, typeof a.putEncryptorUsingPUT === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.putEncryptorUsingPUT(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putEncryptorUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.putEncryptorUsingPUT('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putEncryptorUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing encryptor', (done) => {
        try {
          a.putEncryptorUsingPUT('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'encryptor is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putEncryptorUsingPUT', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEncryptorUsingDELETE - errors', () => {
      it('should have a deleteEncryptorUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteEncryptorUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteEncryptorUsingDELETE(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteEncryptorUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.deleteEncryptorUsingDELETE('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteEncryptorUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchEncryptorUsingPATCH - errors', () => {
      it('should have a patchEncryptorUsingPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.patchEncryptorUsingPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.patchEncryptorUsingPATCH(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchEncryptorUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceName', (done) => {
        try {
          a.patchEncryptorUsingPATCH('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'deviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchEncryptorUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing patch', (done) => {
        try {
          a.patchEncryptorUsingPATCH('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'patch is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchEncryptorUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateInternetNodeUsingPATCH - errors', () => {
      it('should have a updateInternetNodeUsingPATCH function', (done) => {
        try {
          assert.equal(true, typeof a.updateInternetNodeUsingPATCH === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateInternetNodeUsingPATCH(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-updateInternetNodeUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing patch', (done) => {
        try {
          a.updateInternetNodeUsingPATCH('fakeparam', null, (data, error) => {
            try {
              const displayE = 'patch is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-updateInternetNodeUsingPATCH', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInternetNodeConnectionsUsingGET - errors', () => {
      it('should have a getInternetNodeConnectionsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getInternetNodeConnectionsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getInternetNodeConnectionsUsingGET(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getInternetNodeConnectionsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInternetNodeConnectionsUsingDELETE - errors', () => {
      it('should have a deleteInternetNodeConnectionsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInternetNodeConnectionsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteInternetNodeConnectionsUsingDELETE(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteInternetNodeConnectionsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntranetNodeConnectionsUsingGET - errors', () => {
      it('should have a getIntranetNodeConnectionsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getIntranetNodeConnectionsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getIntranetNodeConnectionsUsingGET(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getIntranetNodeConnectionsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.getIntranetNodeConnectionsUsingGET('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getIntranetNodeConnectionsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uplinkDevice', (done) => {
        try {
          a.getIntranetNodeConnectionsUsingGET('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'uplinkDevice is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getIntranetNodeConnectionsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uplinkPort', (done) => {
        try {
          a.getIntranetNodeConnectionsUsingGET('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'uplinkPort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getIntranetNodeConnectionsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayDevice', (done) => {
        try {
          a.getIntranetNodeConnectionsUsingGET('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'gatewayDevice is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getIntranetNodeConnectionsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayPort', (done) => {
        try {
          a.getIntranetNodeConnectionsUsingGET('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'gatewayPort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getIntranetNodeConnectionsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vlan', (done) => {
        try {
          a.getIntranetNodeConnectionsUsingGET('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vlan is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getIntranetNodeConnectionsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIntranetNodeConnectionsUsingDELETE - errors', () => {
      it('should have a deleteIntranetNodeConnectionsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIntranetNodeConnectionsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteIntranetNodeConnectionsUsingDELETE(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteIntranetNodeConnectionsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.deleteIntranetNodeConnectionsUsingDELETE('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteIntranetNodeConnectionsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uplinkDevice', (done) => {
        try {
          a.deleteIntranetNodeConnectionsUsingDELETE('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'uplinkDevice is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteIntranetNodeConnectionsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uplinkPort', (done) => {
        try {
          a.deleteIntranetNodeConnectionsUsingDELETE('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'uplinkPort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteIntranetNodeConnectionsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayDevice', (done) => {
        try {
          a.deleteIntranetNodeConnectionsUsingDELETE('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'gatewayDevice is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteIntranetNodeConnectionsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayPort', (done) => {
        try {
          a.deleteIntranetNodeConnectionsUsingDELETE('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'gatewayPort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteIntranetNodeConnectionsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vlan', (done) => {
        try {
          a.deleteIntranetNodeConnectionsUsingDELETE('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vlan is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteIntranetNodeConnectionsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL2VpnConnectionsUsingGET - errors', () => {
      it('should have a getL2VpnConnectionsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getL2VpnConnectionsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getL2VpnConnectionsUsingGET(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL2VpnConnectionsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l2VpnName', (done) => {
        try {
          a.getL2VpnConnectionsUsingGET('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'l2VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL2VpnConnectionsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getL2VpnConnectionsUsingGET('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL2VpnConnectionsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing port', (done) => {
        try {
          a.getL2VpnConnectionsUsingGET('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'port is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL2VpnConnectionsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vlan', (done) => {
        try {
          a.getL2VpnConnectionsUsingGET('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vlan is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL2VpnConnectionsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteL2VpnConnectionsUsingDELETE - errors', () => {
      it('should have a deleteL2VpnConnectionsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteL2VpnConnectionsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteL2VpnConnectionsUsingDELETE(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL2VpnConnectionsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l2VpnName', (done) => {
        try {
          a.deleteL2VpnConnectionsUsingDELETE('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'l2VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL2VpnConnectionsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.deleteL2VpnConnectionsUsingDELETE('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL2VpnConnectionsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing port', (done) => {
        try {
          a.deleteL2VpnConnectionsUsingDELETE('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'port is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL2VpnConnectionsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vlan', (done) => {
        try {
          a.deleteL2VpnConnectionsUsingDELETE('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vlan is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL2VpnConnectionsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL2VpnsUsingGETv1 - errors', () => {
      it('should have a getL2VpnsUsingGETv1 function', (done) => {
        try {
          assert.equal(true, typeof a.getL2VpnsUsingGETv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getL2VpnsUsingGETv1(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL2VpnsUsingGETv1Query', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putL2VpnsUsingPUTv1 - errors', () => {
      it('should have a putL2VpnsUsingPUTv1 function', (done) => {
        try {
          assert.equal(true, typeof a.putL2VpnsUsingPUTv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.putL2VpnsUsingPUTv1(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putL2VpnsUsingPUTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l2VpnList', (done) => {
        try {
          a.putL2VpnsUsingPUTv1('fakeparam', null, (data, error) => {
            try {
              const displayE = 'l2VpnList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putL2VpnsUsingPUTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL2VpnUsingGETv1 - errors', () => {
      it('should have a getL2VpnUsingGETv1 function', (done) => {
        try {
          assert.equal(true, typeof a.getL2VpnUsingGETv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getL2VpnUsingGETv1(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL2VpnUsingGETv1Query', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l2VpnName', (done) => {
        try {
          a.getL2VpnUsingGETv1('fakeparam', null, (data, error) => {
            try {
              const displayE = 'l2VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL2VpnUsingGETv1Query', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putL2VpnUsingPUTv1 - errors', () => {
      it('should have a putL2VpnUsingPUTv1 function', (done) => {
        try {
          assert.equal(true, typeof a.putL2VpnUsingPUTv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.putL2VpnUsingPUTv1(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putL2VpnUsingPUTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l2VpnName', (done) => {
        try {
          a.putL2VpnUsingPUTv1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'l2VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putL2VpnUsingPUTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l2Vpn', (done) => {
        try {
          a.putL2VpnUsingPUTv1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'l2Vpn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putL2VpnUsingPUTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteL2VpnUsingDELETEv1 - errors', () => {
      it('should have a deleteL2VpnUsingDELETEv1 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteL2VpnUsingDELETEv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteL2VpnUsingDELETEv1(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL2VpnUsingDELETEv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l2VpnName', (done) => {
        try {
          a.deleteL2VpnUsingDELETEv1('fakeparam', null, (data, error) => {
            try {
              const displayE = 'l2VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL2VpnUsingDELETEv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchL2VpnUsingPATCHv1 - errors', () => {
      it('should have a patchL2VpnUsingPATCHv1 function', (done) => {
        try {
          assert.equal(true, typeof a.patchL2VpnUsingPATCHv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.patchL2VpnUsingPATCHv1(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchL2VpnUsingPATCHv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l2VpnName', (done) => {
        try {
          a.patchL2VpnUsingPATCHv1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'l2VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchL2VpnUsingPATCHv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing patch', (done) => {
        try {
          a.patchL2VpnUsingPATCHv1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'patch is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchL2VpnUsingPATCHv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addL2VpnConnectionUsingPOSTv1 - errors', () => {
      it('should have a addL2VpnConnectionUsingPOSTv1 function', (done) => {
        try {
          assert.equal(true, typeof a.addL2VpnConnectionUsingPOSTv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.addL2VpnConnectionUsingPOSTv1(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addL2VpnConnectionUsingPOSTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l2VpnName', (done) => {
        try {
          a.addL2VpnConnectionUsingPOSTv1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'l2VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addL2VpnConnectionUsingPOSTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connection', (done) => {
        try {
          a.addL2VpnConnectionUsingPOSTv1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connection is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addL2VpnConnectionUsingPOSTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL3VpnConnectionsUsingGET - errors', () => {
      it('should have a getL3VpnConnectionsUsingGET function', (done) => {
        try {
          assert.equal(true, typeof a.getL3VpnConnectionsUsingGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getL3VpnConnectionsUsingGET(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL3VpnConnectionsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l3VpnName', (done) => {
        try {
          a.getL3VpnConnectionsUsingGET('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'l3VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL3VpnConnectionsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uplinkDevice', (done) => {
        try {
          a.getL3VpnConnectionsUsingGET('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'uplinkDevice is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL3VpnConnectionsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uplinkPort', (done) => {
        try {
          a.getL3VpnConnectionsUsingGET('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'uplinkPort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL3VpnConnectionsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayDevice', (done) => {
        try {
          a.getL3VpnConnectionsUsingGET('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'gatewayDevice is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL3VpnConnectionsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayPort', (done) => {
        try {
          a.getL3VpnConnectionsUsingGET('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'gatewayPort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL3VpnConnectionsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vlan', (done) => {
        try {
          a.getL3VpnConnectionsUsingGET('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vlan is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL3VpnConnectionsUsingGETQuery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteL3VpnConnectionsUsingDELETE - errors', () => {
      it('should have a deleteL3VpnConnectionsUsingDELETE function', (done) => {
        try {
          assert.equal(true, typeof a.deleteL3VpnConnectionsUsingDELETE === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteL3VpnConnectionsUsingDELETE(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL3VpnConnectionsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l3VpnName', (done) => {
        try {
          a.deleteL3VpnConnectionsUsingDELETE('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'l3VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL3VpnConnectionsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uplinkDevice', (done) => {
        try {
          a.deleteL3VpnConnectionsUsingDELETE('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'uplinkDevice is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL3VpnConnectionsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uplinkPort', (done) => {
        try {
          a.deleteL3VpnConnectionsUsingDELETE('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'uplinkPort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL3VpnConnectionsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayDevice', (done) => {
        try {
          a.deleteL3VpnConnectionsUsingDELETE('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'gatewayDevice is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL3VpnConnectionsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayPort', (done) => {
        try {
          a.deleteL3VpnConnectionsUsingDELETE('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'gatewayPort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL3VpnConnectionsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vlan', (done) => {
        try {
          a.deleteL3VpnConnectionsUsingDELETE('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vlan is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL3VpnConnectionsUsingDELETE', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL3VpnsUsingGETv1 - errors', () => {
      it('should have a getL3VpnsUsingGETv1 function', (done) => {
        try {
          assert.equal(true, typeof a.getL3VpnsUsingGETv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getL3VpnsUsingGETv1(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL3VpnsUsingGETv1Query', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putL3VpnsUsingPUTv1 - errors', () => {
      it('should have a putL3VpnsUsingPUTv1 function', (done) => {
        try {
          assert.equal(true, typeof a.putL3VpnsUsingPUTv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.putL3VpnsUsingPUTv1(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putL3VpnsUsingPUTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l3VpnList', (done) => {
        try {
          a.putL3VpnsUsingPUTv1('fakeparam', null, (data, error) => {
            try {
              const displayE = 'l3VpnList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putL3VpnsUsingPUTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL3VpnUsingGETv1 - errors', () => {
      it('should have a getL3VpnUsingGETv1 function', (done) => {
        try {
          assert.equal(true, typeof a.getL3VpnUsingGETv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getL3VpnUsingGETv1(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL3VpnUsingGETv1Query', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l3VpnName', (done) => {
        try {
          a.getL3VpnUsingGETv1('fakeparam', null, (data, error) => {
            try {
              const displayE = 'l3VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getL3VpnUsingGETv1Query', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putL3VpnUsingPUTv1 - errors', () => {
      it('should have a putL3VpnUsingPUTv1 function', (done) => {
        try {
          assert.equal(true, typeof a.putL3VpnUsingPUTv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.putL3VpnUsingPUTv1(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putL3VpnUsingPUTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l3VpnName', (done) => {
        try {
          a.putL3VpnUsingPUTv1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'l3VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putL3VpnUsingPUTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l3Vpn', (done) => {
        try {
          a.putL3VpnUsingPUTv1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'l3Vpn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putL3VpnUsingPUTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteL3VpnUsingDELETEv1 - errors', () => {
      it('should have a deleteL3VpnUsingDELETEv1 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteL3VpnUsingDELETEv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteL3VpnUsingDELETEv1(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL3VpnUsingDELETEv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l3VpnName', (done) => {
        try {
          a.deleteL3VpnUsingDELETEv1('fakeparam', null, (data, error) => {
            try {
              const displayE = 'l3VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteL3VpnUsingDELETEv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchL3VpnUsingPATCHv1 - errors', () => {
      it('should have a patchL3VpnUsingPATCHv1 function', (done) => {
        try {
          assert.equal(true, typeof a.patchL3VpnUsingPATCHv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.patchL3VpnUsingPATCHv1(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchL3VpnUsingPATCHv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l3VpnName', (done) => {
        try {
          a.patchL3VpnUsingPATCHv1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'l3VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchL3VpnUsingPATCHv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing patch', (done) => {
        try {
          a.patchL3VpnUsingPATCHv1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'patch is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchL3VpnUsingPATCHv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addL3VpnConnectionUsingPOSTv1 - errors', () => {
      it('should have a addL3VpnConnectionUsingPOSTv1 function', (done) => {
        try {
          assert.equal(true, typeof a.addL3VpnConnectionUsingPOSTv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.addL3VpnConnectionUsingPOSTv1(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addL3VpnConnectionUsingPOSTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing l3VpnName', (done) => {
        try {
          a.addL3VpnConnectionUsingPOSTv1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'l3VpnName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addL3VpnConnectionUsingPOSTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connection', (done) => {
        try {
          a.addL3VpnConnectionUsingPOSTv1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connection is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addL3VpnConnectionUsingPOSTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInternetNodeUsingGETv1 - errors', () => {
      it('should have a getInternetNodeUsingGETv1 function', (done) => {
        try {
          assert.equal(true, typeof a.getInternetNodeUsingGETv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getInternetNodeUsingGETv1(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getInternetNodeUsingGETv1Query', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putInternetNodeUsingPUTv1 - errors', () => {
      it('should have a putInternetNodeUsingPUTv1 function', (done) => {
        try {
          assert.equal(true, typeof a.putInternetNodeUsingPUTv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.putInternetNodeUsingPUTv1(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putInternetNodeUsingPUTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing internetNode', (done) => {
        try {
          a.putInternetNodeUsingPUTv1('fakeparam', null, (data, error) => {
            try {
              const displayE = 'internetNode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putInternetNodeUsingPUTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addInternetNodeConnectionUsingPOSTv1 - errors', () => {
      it('should have a addInternetNodeConnectionUsingPOSTv1 function', (done) => {
        try {
          assert.equal(true, typeof a.addInternetNodeConnectionUsingPOSTv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.addInternetNodeConnectionUsingPOSTv1(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addInternetNodeConnectionUsingPOSTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connection', (done) => {
        try {
          a.addInternetNodeConnectionUsingPOSTv1('fakeparam', null, (data, error) => {
            try {
              const displayE = 'connection is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addInternetNodeConnectionUsingPOSTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntranetNodesUsingGETv1 - errors', () => {
      it('should have a getIntranetNodesUsingGETv1 function', (done) => {
        try {
          assert.equal(true, typeof a.getIntranetNodesUsingGETv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getIntranetNodesUsingGETv1(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getIntranetNodesUsingGETv1Query', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIntranetNodesUsingPUTv1 - errors', () => {
      it('should have a putIntranetNodesUsingPUTv1 function', (done) => {
        try {
          assert.equal(true, typeof a.putIntranetNodesUsingPUTv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.putIntranetNodesUsingPUTv1(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putIntranetNodesUsingPUTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing intranetNodeList', (done) => {
        try {
          a.putIntranetNodesUsingPUTv1('fakeparam', null, (data, error) => {
            try {
              const displayE = 'intranetNodeList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putIntranetNodesUsingPUTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntranetNodeUsingGETv1 - errors', () => {
      it('should have a getIntranetNodeUsingGETv1 function', (done) => {
        try {
          assert.equal(true, typeof a.getIntranetNodeUsingGETv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getIntranetNodeUsingGETv1(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getIntranetNodeUsingGETv1Query', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.getIntranetNodeUsingGETv1('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getIntranetNodeUsingGETv1Query', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIntranetNodeUsingPUTv1 - errors', () => {
      it('should have a putIntranetNodeUsingPUTv1 function', (done) => {
        try {
          assert.equal(true, typeof a.putIntranetNodeUsingPUTv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.putIntranetNodeUsingPUTv1(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putIntranetNodeUsingPUTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.putIntranetNodeUsingPUTv1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putIntranetNodeUsingPUTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing intranetNode', (done) => {
        try {
          a.putIntranetNodeUsingPUTv1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'intranetNode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putIntranetNodeUsingPUTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIntranetNodeUsingDELETEv1 - errors', () => {
      it('should have a deleteIntranetNodeUsingDELETEv1 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteIntranetNodeUsingDELETEv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteIntranetNodeUsingDELETEv1(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteIntranetNodeUsingDELETEv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.deleteIntranetNodeUsingDELETEv1('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteIntranetNodeUsingDELETEv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIntranetNodeUsingPATCHv1 - errors', () => {
      it('should have a patchIntranetNodeUsingPATCHv1 function', (done) => {
        try {
          assert.equal(true, typeof a.patchIntranetNodeUsingPATCHv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.patchIntranetNodeUsingPATCHv1(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchIntranetNodeUsingPATCHv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.patchIntranetNodeUsingPATCHv1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchIntranetNodeUsingPATCHv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing patch', (done) => {
        try {
          a.patchIntranetNodeUsingPATCHv1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'patch is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchIntranetNodeUsingPATCHv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addIntranetNodeConnectionUsingPOSTv1 - errors', () => {
      it('should have a addIntranetNodeConnectionUsingPOSTv1 function', (done) => {
        try {
          assert.equal(true, typeof a.addIntranetNodeConnectionUsingPOSTv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.addIntranetNodeConnectionUsingPOSTv1(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addIntranetNodeConnectionUsingPOSTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nodeName', (done) => {
        try {
          a.addIntranetNodeConnectionUsingPOSTv1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nodeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addIntranetNodeConnectionUsingPOSTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connection', (done) => {
        try {
          a.addIntranetNodeConnectionUsingPOSTv1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'connection is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-addIntranetNodeConnectionUsingPOSTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWanCircuitsUsingGETv1 - errors', () => {
      it('should have a getWanCircuitsUsingGETv1 function', (done) => {
        try {
          assert.equal(true, typeof a.getWanCircuitsUsingGETv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getWanCircuitsUsingGETv1(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getWanCircuitsUsingGETv1Query', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWanCircuitsUsingPUTv1 - errors', () => {
      it('should have a putWanCircuitsUsingPUTv1 function', (done) => {
        try {
          assert.equal(true, typeof a.putWanCircuitsUsingPUTv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.putWanCircuitsUsingPUTv1(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putWanCircuitsUsingPUTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wanCircuitList', (done) => {
        try {
          a.putWanCircuitsUsingPUTv1('fakeparam', null, (data, error) => {
            try {
              const displayE = 'wanCircuitList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putWanCircuitsUsingPUTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWanCircuitUsingGETv1 - errors', () => {
      it('should have a getWanCircuitUsingGETv1 function', (done) => {
        try {
          assert.equal(true, typeof a.getWanCircuitUsingGETv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getWanCircuitUsingGETv1(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getWanCircuitUsingGETv1Query', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wanCircuitName', (done) => {
        try {
          a.getWanCircuitUsingGETv1('fakeparam', null, (data, error) => {
            try {
              const displayE = 'wanCircuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-getWanCircuitUsingGETv1Query', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWanCircuitUsingPUTv1 - errors', () => {
      it('should have a putWanCircuitUsingPUTv1 function', (done) => {
        try {
          assert.equal(true, typeof a.putWanCircuitUsingPUTv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.putWanCircuitUsingPUTv1(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putWanCircuitUsingPUTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wanCircuitName', (done) => {
        try {
          a.putWanCircuitUsingPUTv1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'wanCircuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putWanCircuitUsingPUTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wanCircuit', (done) => {
        try {
          a.putWanCircuitUsingPUTv1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'wanCircuit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-putWanCircuitUsingPUTv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWanCircuitUsingDELETEv1 - errors', () => {
      it('should have a deleteWanCircuitUsingDELETEv1 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWanCircuitUsingDELETEv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteWanCircuitUsingDELETEv1(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteWanCircuitUsingDELETEv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wanCircuitName', (done) => {
        try {
          a.deleteWanCircuitUsingDELETEv1('fakeparam', null, (data, error) => {
            try {
              const displayE = 'wanCircuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-deleteWanCircuitUsingDELETEv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchWanCircuitUsingPATCHv1 - errors', () => {
      it('should have a patchWanCircuitUsingPATCHv1 function', (done) => {
        try {
          assert.equal(true, typeof a.patchWanCircuitUsingPATCHv1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.patchWanCircuitUsingPATCHv1(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchWanCircuitUsingPATCHv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wanCircuitName', (done) => {
        try {
          a.patchWanCircuitUsingPATCHv1('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'wanCircuitName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchWanCircuitUsingPATCHv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing patch', (done) => {
        try {
          a.patchWanCircuitUsingPATCHv1('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'patch is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-forwardnetworks-adapter-patchWanCircuitUsingPATCHv1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
