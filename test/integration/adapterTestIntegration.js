/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-forwardnetworks',
      type: 'ForwardNetworks',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const ForwardNetworks = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] ForwardNetworks Adapter Test', () => {
  describe('ForwardNetworks Class Tests', () => {
    const a = new ForwardNetworks(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    let networksName = 'fakedata';
    let networksNetworkId = 'fakedata';
    describe('#createNetworkUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNetworkUsingPOST(networksName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.creatorId);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.orgId);
              } else {
                runCommonAsserts(data, error);
              }
              networksName = data.response.name;
              networksNetworkId = data.response.id;
              saveMockData('Networks', 'createNetworkUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworksUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworksUsingGET((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'getNetworksUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networksUpdateNetworkUsingPATCHBodyParam = {
      name: 'string'
    };
    describe('#updateNetworkUsingPATCH - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkUsingPATCH(networksNetworkId, networksUpdateNetworkUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'updateNetworkUsingPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkCollectionNetworkId = 'fakedata';
    describe('#cancelCollectUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cancelCollectUsingPOST(networkCollectionNetworkId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkCollection', 'cancelCollectUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkCollectionCollectUsingPOSTBodyParam = {
      deviceGroup: 'ATL_devices',
      devices: [
        'atl-r*-??',
        'sjc-r*-??'
      ]
    };
    describe('#collectUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.collectUsingPOST(networkCollectionNetworkId, networkCollectionCollectUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkCollection', 'collectUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCollectorStateUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCollectorStateUsingGET(networkCollectionNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('UPDATING', data.response.busyStatus);
                assert.equal(false, data.response.hasDevicesConfigured);
                assert.equal(false, data.response.isIdle);
                assert.equal(true, data.response.isOnline);
                assert.equal(false, data.response.isSet);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkCollection', 'getCollectorStateUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let networkSetupNetworkId = 'fakedata';
    let networkSetupCredentialId = 'fakedata';
    const networkSetupCreateDeviceCredentialUsingPOSTBodyParam = {
      type: 'LOGIN',
      name: 'admin (sjc)',
      password: samProps.authentication.password
    };
    describe('#createDeviceCredentialUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeviceCredentialUsingPOST(networkSetupNetworkId, networkSetupCreateDeviceCredentialUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('8c4a168e-c6ca-4978-bcc9-c0ec7f64a163', data.response.id);
                assert.equal('LOGIN', data.response.type);
                assert.equal('admin (sjc)', data.response.name);
                assert.equal('admin', data.response.username);
                assert.equal('my-s3cr3t-p4s$w0rd', data.response.password);
                assert.equal('MIIPIQIBAzCCDtoGCSqGSIb3DQEHAa...', data.response.content);
              } else {
                runCommonAsserts(data, error);
              }
              networkSetupNetworkId = data.response.id;
              networkSetupCredentialId = data.response.id;
              saveMockData('NetworkSetup', 'createDeviceCredentialUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSetupAddOrUpdateDeviceSourcesUsingPOSTBodyParam = [
      {
        collectBgpAdvertisements: false,
        disableCollection: true,
        fullCollectionLog: true,
        host: '10.121.7.13',
        jumpServerId: 'my-sjc-jump-server',
        keyStoreId: 'my-tls-key-store',
        largeRtt: true,
        loginCredentialId: 'my-checkpoint-credentials',
        name: 'my_router',
        paginationMode: 'DISABLE_PAGINATION',
        port: 22,
        privilegedModePasswordId: samProps.authentication.password,
        shellCredentialId: 'my_avi_shell_cred',
        type: 'checkpoint_ssh',
        openFlow: {
          port: 6653
        }
      }
    ];
    describe('#addOrUpdateDeviceSourcesUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addOrUpdateDeviceSourcesUsingPOST(networkSetupNetworkId, networkSetupAddOrUpdateDeviceSourcesUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSetup', 'addOrUpdateDeviceSourcesUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSetupCreateJumpServerUsingPOSTBodyParam = {
      host: '10.121.7.14',
      username: 'admin',
      password: samProps.authentication.password
    };
    describe('#createJumpServerUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createJumpServerUsingPOST(networkSetupNetworkId, networkSetupCreateJumpServerUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('8c4a168e-c6ca-4978-bcc9-c0ec7f64a164', data.response.id);
                assert.equal('10.121.7.14', data.response.host);
                assert.equal(23, data.response.port);
                assert.equal('admin', data.response.username);
                assert.equal('my-s3cr3t-p4s$w0rd', data.response.password);
                assert.equal(false, data.response.supportsPortForwarding);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSetup', 'createJumpServerUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCredentialsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceCredentialsUsingGET(networkSetupNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSetup', 'getDeviceCredentialsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSetupPatchDeviceCredentialUsingPATCHBodyParam = {
      type: 'LOGIN'
    };
    describe('#patchDeviceCredentialUsingPATCH - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchDeviceCredentialUsingPATCH(networkSetupNetworkId, networkSetupCredentialId, networkSetupPatchDeviceCredentialUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSetup', 'patchDeviceCredentialUsingPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceSourcesUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceSourcesUsingGET(networkSetupNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSetup', 'getDeviceSourcesUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSetupDeviceSourceName = 'fakedata';
    const networkSetupUpdateDeviceSourceUsingPUTBodyParam = {
      host: '10.121.7.13',
      name: 'my_router',
      type: 'checkpoint_ssh'
    };
    describe('#updateDeviceSourceUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDeviceSourceUsingPUT(networkSetupNetworkId, networkSetupDeviceSourceName, networkSetupUpdateDeviceSourceUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSetup', 'updateDeviceSourceUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJumpServersUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJumpServersUsingGET(networkSetupNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSetup', 'getJumpServersUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSetupJumpServerId = 'fakedata';
    const networkSetupEditJumpServerUsingPATCHBodyParam = {
      host: '10.121.7.14',
      port: 23,
      username: 'admin',
      password: samProps.authentication.password,
      supportsPortForwarding: true
    };
    describe('#editJumpServerUsingPATCH - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.editJumpServerUsingPATCH(networkSetupNetworkId, networkSetupJumpServerId, networkSetupEditJumpServerUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSetup', 'editJumpServerUsingPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkLayoutNetworkId = 'fakedata';
    const networkLayoutSetNetworkLayoutUsingPOSTBodyParam = {
      nodes: [
        {
          id: 'string',
          loopbackDirection: 4,
          x: 2,
          y: 10
        }
      ]
    };
    describe('#setNetworkLayoutUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setNetworkLayoutUsingPOST(networkLayoutNetworkId, networkLayoutSetNetworkLayoutUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkLayout', 'setNetworkLayoutUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkLayoutUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkLayoutUsingGET(networkLayoutNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.nodes));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkLayout', 'getNetworkLayoutUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let networkSnapshotsNetworkId = 'fakedata';
    const networkSnapshotsFile = 'fakedata';
    describe('#createSnapshotUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSnapshotUsingPOST(networkSnapshotsFile, networkSnapshotsNetworkId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1569001234567, data.response.creationDateMillis);
                assert.equal('string', data.response.id);
                assert.equal(true, data.response.isDraft);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.note);
                assert.equal('string', data.response.parentSnapshotId);
                assert.equal(1569003456789, data.response.processedAtMillis);
                assert.equal('OK', data.response.processingStatus);
                assert.equal('REPROCESS', data.response.processingTrigger);
                assert.equal('OK', data.response.snapshotCollectionStatus);
                assert.equal('NO_WARNINGS', data.response.supportVerifierStatus);
              } else {
                runCommonAsserts(data, error);
              }
              networkSnapshotsNetworkId = data.response.id;
              saveMockData('NetworkSnapshots', 'createSnapshotUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSnapshotsFiles = ['fakedata'];
    describe('#importSnapshotWithFilesUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.importSnapshotWithFilesUsingPOST(networkSnapshotsFiles, networkSnapshotsNetworkId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1569001234567, data.response.creationDateMillis);
                assert.equal('string', data.response.id);
                assert.equal(true, data.response.isDraft);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.note);
                assert.equal('string', data.response.parentSnapshotId);
                assert.equal(1569003456789, data.response.processedAtMillis);
                assert.equal('OK', data.response.processingStatus);
                assert.equal('REPROCESS', data.response.processingTrigger);
                assert.equal('OK', data.response.snapshotCollectionStatus);
                assert.equal('NO_WARNINGS', data.response.supportVerifierStatus);
              } else {
                runCommonAsserts(data, error);
              }
              networkSnapshotsNetworkId = data.response.id;
              saveMockData('NetworkSnapshots', 'importSnapshotWithFilesUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listNetworkSnapshotsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listNetworkSnapshotsUsingGET(networkSnapshotsNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.creatorId);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.orgId);
                assert.equal(true, Array.isArray(data.response.snapshots));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSnapshots', 'listNetworkSnapshotsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLatestProcessedSnapshotUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLatestProcessedSnapshotUsingGET(networkSnapshotsNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1569001234567, data.response.creationDateMillis);
                assert.equal('string', data.response.id);
                assert.equal(false, data.response.isDraft);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.note);
                assert.equal('string', data.response.parentSnapshotId);
                assert.equal(1569003456789, data.response.processedAtMillis);
                assert.equal('OK', data.response.processingStatus);
                assert.equal('FORK', data.response.processingTrigger);
                assert.equal('OK', data.response.snapshotCollectionStatus);
                assert.equal('NO_WARNINGS', data.response.supportVerifierStatus);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSnapshots', 'getLatestProcessedSnapshotUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSnapshotsSnapshotId = 'fakedata';
    describe('#zipSnapshotUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.zipSnapshotUsingGET(networkSnapshotsSnapshotId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSnapshots', 'zipSnapshotUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnapshotMetricsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSnapshotMetricsUsingGET(networkSnapshotsSnapshotId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(16, data.response.collectionConcurrency);
                assert.equal(1234, data.response.collectionDuration);
                assert.equal('object', typeof data.response.collectionFailures);
                assert.equal(1234567, data.response.creationDateMillis);
                assert.equal('SUCCESS', data.response.hostComputationStatus);
                assert.equal('SUCCESS', data.response.ipLocationIndexingStatus);
                assert.equal(6, data.response.jumpServerCollectionConcurrency);
                assert.equal('SUCCESS', data.response.l2IndexingStatus);
                assert.equal(false, data.response.needsReprocessing);
                assert.equal(4, data.response.numCollectionFailureDevices);
                assert.equal(1, data.response.numParsingFailureDevices);
                assert.equal(401, data.response.numSuccessfulDevices);
                assert.equal('object', typeof data.response.parsingFailures);
                assert.equal('SUCCESS', data.response.pathSearchIndexingStatus);
                assert.equal(5678, data.response.processingDuration);
                assert.equal('SUCCESS', data.response.searchIndexingStatus);
                assert.equal('string', data.response.snapshotId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSnapshots', 'getSnapshotMetricsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkTopologySnapshotId = 'fakedata';
    const networkTopologyPostSnapshotTopoOverridesUsingPOSTBodyParam = {
      presentAdditions: [
        {
          port1: 'string',
          port2: 'string',
          vlan: 6
        }
      ],
      presentRemovals: [
        {
          port1: 'string',
          port2: 'string'
        }
      ],
      absentAdditions: [
        {
          port1: 'string',
          port2: 'string'
        }
      ],
      absentRemovals: [
        {
          port1: 'string',
          port2: 'string'
        }
      ]
    };
    describe('#postSnapshotTopoOverridesUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSnapshotTopoOverridesUsingPOST(networkTopologySnapshotId, networkTopologyPostSnapshotTopoOverridesUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkTopology', 'postSnapshotTopoOverridesUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkTopologyNetworkId = 'fakedata';
    const networkTopologySetNetworkTopoListUsingPUTBodyParam = [
      [
        'string',
        'string',
        'string',
        'string',
        'string',
        'string',
        'string'
      ]
    ];
    describe('#setNetworkTopoListUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setNetworkTopoListUsingPUT(networkTopologyNetworkId, networkTopologySetNetworkTopoListUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkTopology', 'setNetworkTopoListUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTopologyUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTopologyUsingGET(networkTopologySnapshotId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkTopology', 'getTopologyUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkTopologyPutSnapshotTopoOverridesUsingPUTBodyParam = {
      absent: [
        {
          port1: 'string',
          port2: 'string'
        }
      ],
      present: [
        {
          port1: 'string',
          port2: 'string'
        }
      ]
    };
    describe('#putSnapshotTopoOverridesUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putSnapshotTopoOverridesUsingPUT(networkTopologySnapshotId, networkTopologyPutSnapshotTopoOverridesUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkTopology', 'putSnapshotTopoOverridesUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnapshotTopoOverridesUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSnapshotTopoOverridesUsingGET(networkTopologySnapshotId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.absent));
                assert.equal(true, Array.isArray(data.response.present));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkTopology', 'getSnapshotTopoOverridesUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const nQERunNqeQueryUsingPOSTBodyParam = {
      query: 'foreach device in network.devices\nforeach i in device.interfaces\nwhere i.adminStatus == AdminStatus.UP\n   && i.operStatus != OperStatus.UP\nselect {\n  deviceName: deviceName,\n  interfaceName: i.name,\n  adminStatus: i.adminStatus,\n  operStatus: i.operStatus\n}',
      queryOptions: {
        columnFilters: [
          {
            columnName: 'deviceName',
            value: 'ATL'
          }
        ],
        limit: 100,
        offset: 20,
        sortBy: {
          columnName: 'deviceName',
          order: 'ASC'
        }
      }
    };
    describe('#runNqeQueryUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.runNqeQueryUsingPOST(null, null, nQERunNqeQueryUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('101', data.response.snapshotId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NQE', 'runNqeQueryUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let checksSnapshotId = 'fakedata';
    let checksCheckId = 'fakedata';
    const checksAddCheckUsingPOSTBodyParam = {};
    describe('#addCheckUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addCheckUsingPOST(checksSnapshotId, null, checksAddCheckUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.creationDateMillis);
                assert.equal('string', data.response.creatorId);
                assert.equal('object', typeof data.response.definition);
                assert.equal('string', data.response.definitionDateMillis);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.editDateMillis);
                assert.equal('string', data.response.editorId);
                assert.equal(false, data.response.enabled);
                assert.equal('string', data.response.executionDateMillis);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.note);
                assert.equal('FAIL', data.response.status);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              checksSnapshotId = data.response.id;
              checksCheckId = data.response.id;
              saveMockData('Checks', 'addCheckUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAvailablePredefinedChecksUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAvailablePredefinedChecksUsingGET((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Checks', 'getAvailablePredefinedChecksUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const checksType = [];
    describe('#getChecksUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getChecksUsingGET(checksSnapshotId, checksType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Checks', 'getChecksUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSingleCheckUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSingleCheckUsingGET(checksSnapshotId, checksCheckId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.creationDateMillis);
                assert.equal('string', data.response.creatorId);
                assert.equal('object', typeof data.response.definition);
                assert.equal('string', data.response.definitionDateMillis);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.diagnosis);
                assert.equal('string', data.response.editDateMillis);
                assert.equal('string', data.response.editorId);
                assert.equal(true, data.response.enabled);
                assert.equal('string', data.response.executionDateMillis);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.note);
                assert.equal('FAIL', data.response.status);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Checks', 'getSingleCheckUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aliasesSnapshotId = 'fakedata';
    describe('#getAllAliasesUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllAliasesUsingGET(aliasesSnapshotId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.aliases));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Aliases', 'getAllAliasesUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aliasesCreateSnapshotAliasUsingPUTBodyParam = {
      name: 'string',
      type: 'HEADERS'
    };
    describe('#createSnapshotAliasUsingPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSnapshotAliasUsingPUT(aliasesSnapshotId, 'fakedata', aliasesCreateSnapshotAliasUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Aliases', 'createSnapshotAliasUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSingleAliasUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSingleAliasUsingGET(aliasesSnapshotId, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.creationTime);
                assert.equal('string', data.response.creatorId);
                assert.equal('string', data.response.name);
                assert.equal('HEADERS', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Aliases', 'getSingleAliasUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSnapshotDataFilesSnapshotId = 'fakedata';
    const networkSnapshotDataFilesDeviceName = 'fakedata';
    describe('#getDeviceFilesUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceFilesUsingGET(networkSnapshotDataFilesSnapshotId, networkSnapshotDataFilesDeviceName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.files));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSnapshotDataFiles', 'getDeviceFilesUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSnapshotDataFilesFileName = 'fakedata';
    describe('#getDeviceFileContentUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeviceFileContentUsingGET(networkSnapshotDataFilesSnapshotId, networkSnapshotDataFilesDeviceName, networkSnapshotDataFilesFileName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSnapshotDataFiles', 'getDeviceFileContentUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const syntheticDevicesSnapshotId = 'fakedata';
    let syntheticDevicesL2VpnName = 'fakedata';
    let syntheticDevicesDeviceName = 'fakedata';
    let syntheticDevicesPortName = 'fakedata';
    const syntheticDevicesAddL2VpnConnectionUsingPOSTBodyParam = {
      device: 'string',
      vlan: 100,
      port: 'string'
    };
    describe('#addL2VpnConnectionUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addL2VpnConnectionUsingPOST(syntheticDevicesSnapshotId, syntheticDevicesL2VpnName, syntheticDevicesAddL2VpnConnectionUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.connections));
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              syntheticDevicesL2VpnName = data.response.name;
              syntheticDevicesDeviceName = data.response.name;
              syntheticDevicesPortName = data.response.name;
              saveMockData('SyntheticDevices', 'addL2VpnConnectionUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let syntheticDevicesL3VpnName = 'fakedata';
    const syntheticDevicesAddL3VpnConnectionUsingPOSTBodyParam = {
      device: 'string',
      port: 'string',
      providerEdgeIp: '1.1.1.1',
      vlan: 100,
      vrf: 'string'
    };
    describe('#addL3VpnConnectionUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addL3VpnConnectionUsingPOST(syntheticDevicesSnapshotId, syntheticDevicesL3VpnName, syntheticDevicesAddL3VpnConnectionUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.connections));
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              syntheticDevicesL3VpnName = data.response.name;
              saveMockData('SyntheticDevices', 'addL3VpnConnectionUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const syntheticDevicesPutL2VpnsUsingPUTBodyParam = {
      l2Vpns: [
        {
          connections: [
            {
              device: 'string',
              vlan: 100,
              port: 'string'
            }
          ],
          name: 'string'
        }
      ]
    };
    describe('#putL2VpnsUsingPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putL2VpnsUsingPUT(syntheticDevicesSnapshotId, syntheticDevicesPutL2VpnsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'putL2VpnsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL2VpnsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getL2VpnsUsingGET(syntheticDevicesSnapshotId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.l2Vpns));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getL2VpnsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const syntheticDevicesPutL2VpnUsingPUTBodyParam = {
      connections: [
        {
          device: 'string',
          vlan: 100,
          port: 'string'
        }
      ],
      name: 'string'
    };
    describe('#putL2VpnUsingPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putL2VpnUsingPUT(syntheticDevicesSnapshotId, syntheticDevicesL2VpnName, syntheticDevicesPutL2VpnUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'putL2VpnUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const syntheticDevicesPatchL2VpnUsingPATCHBodyParam = {
      name: 'string'
    };
    describe('#patchL2VpnUsingPATCH - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchL2VpnUsingPATCH(syntheticDevicesSnapshotId, syntheticDevicesL2VpnName, syntheticDevicesPatchL2VpnUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'patchL2VpnUsingPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL2VpnUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getL2VpnUsingGET(syntheticDevicesSnapshotId, syntheticDevicesL2VpnName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.connections));
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getL2VpnUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const syntheticDevicesPutL3VpnsUsingPUTBodyParam = {
      l3Vpns: [
        {
          connections: [
            {
              device: 'string',
              port: 'string',
              providerEdgeIp: '1.1.1.1',
              vlan: 100,
              vrf: 'string'
            }
          ],
          name: 'string'
        }
      ]
    };
    describe('#putL3VpnsUsingPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putL3VpnsUsingPUT(syntheticDevicesSnapshotId, syntheticDevicesPutL3VpnsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'putL3VpnsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL3VpnsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getL3VpnsUsingGET(syntheticDevicesSnapshotId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.l3Vpns));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getL3VpnsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const syntheticDevicesPutL3VpnUsingPUTBodyParam = {
      connections: [
        {
          device: 'string',
          port: 'string',
          providerEdgeIp: '1.1.1.1',
          vlan: 100,
          vrf: 'string'
        }
      ],
      name: 'string'
    };
    describe('#putL3VpnUsingPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putL3VpnUsingPUT(syntheticDevicesSnapshotId, syntheticDevicesL3VpnName, syntheticDevicesPutL3VpnUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'putL3VpnUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const syntheticDevicesPatchL3VpnUsingPATCHBodyParam = {
      name: 'string'
    };
    describe('#patchL3VpnUsingPATCH - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchL3VpnUsingPATCH(syntheticDevicesSnapshotId, syntheticDevicesL3VpnName, syntheticDevicesPatchL3VpnUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'patchL3VpnUsingPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL3VpnUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getL3VpnUsingGET(syntheticDevicesSnapshotId, syntheticDevicesL3VpnName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.connections));
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getL3VpnUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pathSearchSnapshotId = 'fakedata';
    const pathSearchGetPathsBulkUsingPOSTBodyParam = {
      queries: [
        {}
      ]
    };
    describe('#getPathsBulkUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPathsBulkUsingPOST(pathSearchSnapshotId, pathSearchGetPathsBulkUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PathSearch', 'getPathsBulkUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pathSearchGetTracePathsBulkSeqUsingPOSTBodyParam = {
      queries: [
        {}
      ]
    };
    describe('#getTracePathsBulkSeqUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTracePathsBulkSeqUsingPOST(pathSearchSnapshotId, pathSearchGetTracePathsBulkSeqUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('MULTICAST', data.response.srcIpLocationType);
                assert.equal('MULTICAST', data.response.dstIpLocationType);
                assert.equal('object', typeof data.response.info);
                assert.equal('object', typeof data.response.returnPathInfo);
                assert.equal(false, data.response.timedOut);
                assert.equal('string', data.response.queryUrl);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PathSearch', 'getTracePathsBulkSeqUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pathSearchSrcIp = 'fakedata';
    const pathSearchDstIp = 'fakedata';
    describe('#getPathsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPathsUsingGET(pathSearchSnapshotId, pathSearchSrcIp, pathSearchDstIp, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('HOST', data.response.srcIpLocationType);
                assert.equal('INTERFACE_ATTACHED_SUBNET', data.response.dstIpLocationType);
                assert.equal('object', typeof data.response.info);
                assert.equal('object', typeof data.response.returnPathInfo);
                assert.equal(false, data.response.timedOut);
                assert.equal('string', data.response.queryUrl);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PathSearch', 'getPathsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPathsByNetworkIdUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPathsByNetworkIdUsingGET('networkId', pathSearchSrcIp, pathSearchDstIp, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PathSearch', 'getPathsByNetworkIdUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPathsBulkByNetworkIdUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPathsBulkByNetworkIdUsingPOST('networkId', 'snapshotId', {}, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PathSearch', 'getPathsBulkByNetworkIdUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTracePathsBulkSeqByNetworkIdUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTracePathsBulkSeqByNetworkIdUsingPOST('networkId', 'snapshotId', {}, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PathSearch', 'getTracePathsBulkSeqByNetworkIdUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL7ApplicationsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getL7ApplicationsUsingGET(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PathSearch', 'getL7ApplicationsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApiVersionUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApiVersionUsingGET((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1.8', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CurrentVersion', 'getApiVersionUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkUsingDELETE - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteNetworkUsingDELETE(networksNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'deleteNetworkUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceCredentialUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDeviceCredentialUsingDELETE(networkSetupNetworkId, networkSetupCredentialId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSetup', 'deleteDeviceCredentialUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceSourceUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDeviceSourceUsingDELETE(networkSetupNetworkId, networkSetupDeviceSourceName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSetup', 'deleteDeviceSourceUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJumpServerUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteJumpServerUsingDELETE(networkSetupNetworkId, networkSetupJumpServerId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSetup', 'deleteJumpServerUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSnapshotUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSnapshotUsingDELETE(networkSnapshotsSnapshotId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSnapshots', 'deleteSnapshotUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateChecksUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deactivateChecksUsingDELETE(checksSnapshotId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Checks', 'deactivateChecksUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateCheckUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deactivateCheckUsingDELETE(checksSnapshotId, checksCheckId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Checks', 'deactivateCheckUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateAliasUsingDELETE - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deactivateAliasUsingDELETE(aliasesSnapshotId, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Aliases', 'deactivateAliasUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteL2VpnUsingDELETE - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteL2VpnUsingDELETE(syntheticDevicesSnapshotId, syntheticDevicesL2VpnName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'deleteL2VpnUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteL2VpnConnectionUsingDELETE - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteL2VpnConnectionUsingDELETE(syntheticDevicesSnapshotId, syntheticDevicesL2VpnName, syntheticDevicesDeviceName, syntheticDevicesPortName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'deleteL2VpnConnectionUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteL3VpnUsingDELETE - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteL3VpnUsingDELETE(syntheticDevicesSnapshotId, syntheticDevicesL3VpnName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'deleteL3VpnUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteL3VpnEdgePortUsingDELETE - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteL3VpnEdgePortUsingDELETE(syntheticDevicesSnapshotId, syntheticDevicesL3VpnName, syntheticDevicesDeviceName, syntheticDevicesPortName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'deleteL3VpnEdgePortUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSetupCreateDeviceCredentialsUsingPATCHBodyParam = [
      {
        id: '8c4a168e-c6ca-4978-bcc9-c0ec7f64a163',
        type: 'LOGIN',
        name: 'admin (sjc)',
        username: 'admin',
        password: samProps.authentication.password,
        content: 'MIIPIQIBAzCCDtoGCSqGSIb3DQEHAa...'
      }
    ];
    describe('#createDeviceCredentialsUsingPATCH - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDeviceCredentialsUsingPATCH('fakedata', networkSetupCreateDeviceCredentialsUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSetup', 'createDeviceCredentialsUsingPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSetupDeleteDeviceSourcesUsingDELETEBodyParam = {
      names: [
        'router_01',
        'router_02'
      ]
    };
    describe('#deleteDeviceSourcesUsingDELETE - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteDeviceSourcesUsingDELETE('fakedata', networkSetupDeleteDeviceSourcesUsingDELETEBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSetup', 'deleteDeviceSourcesUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSnapshotsCustomZipSnapshotUsingPOSTBodyParam = {
      includeDevices: [
        'device-1',
        'device-2'
      ],
      excludeDevices: [
        'device-6',
        'device-7'
      ],
      obfuscationKey: 'a-sEcr3t-kEy-th4t-i$-h4rd-to-guE$$',
      obfuscateNames: false
    };
    describe('#customZipSnapshotUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.customZipSnapshotUsingPOST('fakedata', networkSnapshotsCustomZipSnapshotUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSnapshots', 'customZipSnapshotUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deactivateAliasesUsingDELETE - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deactivateAliasesUsingDELETE('fakedata', null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Aliases', 'deactivateAliasesUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicesUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDevicesUsingGET('fakedata', null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkDevices', 'getDevicesUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOneDeviceUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOneDeviceUsingGET('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('nyc-dc01-fw02', data.response.name);
                assert.equal('nyc-dc01-fw02', data.response.displayName);
                assert.equal('nyc-dc01-fw02', data.response.sourceName);
                assert.equal('FIREWALL', data.response.type);
                assert.equal('F5', data.response.manufacturer);
                assert.equal('BIG-IP Virtual Edition', data.response.model);
                assert.equal('f5', data.response.platform);
                assert.equal('11.6.1', data.response.osVersion);
                assert.equal(true, Array.isArray(data.response.managementIps));
                assert.equal('AUTHENTICATION_FAILED', data.response.collectionError);
                assert.equal('LICENSE_EXHAUSTED', data.response.processingError);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkDevices', 'getOneDeviceUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMissingDevicesUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMissingDevicesUsingGET('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.devices));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkDevices', 'getMissingDevicesUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevicesByNetworkIdUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDevicesByNetworkIdUsingGET('fakedata', null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkDevices', 'getDevicesByNetworkIdUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInternetNodeUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInternetNodeUsingGET('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('inet', data.response.name);
                assert.equal(true, Array.isArray(data.response.connections));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getInternetNodeUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const syntheticDevicesPutInternetNodeUsingPUTBodyParam = {
      name: 'inet',
      connections: [
        {
          gatewayPort: {
            device: 'string',
            port: 'string'
          },
          peerIps: [
            '1.1.1.1'
          ],
          subnetAutoDiscovery: 'BGP_ROUTES',
          subnets: [
            '123.223.47.0/24'
          ],
          uplinkPort: {
            device: 'string',
            port: 'string'
          },
          vlan: 100
        }
      ]
    };
    describe('#putInternetNodeUsingPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putInternetNodeUsingPUT('fakedata', syntheticDevicesPutInternetNodeUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'putInternetNodeUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInternetNodeUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteInternetNodeUsingDELETE('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'deleteInternetNodeUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const syntheticDevicesAddInternetNodeConnectionUsingPOSTBodyParam = {
      gatewayPort: {
        device: 'string',
        port: 'string'
      },
      peerIps: [
        '1.1.1.1'
      ],
      subnetAutoDiscovery: 'IP_ROUTES',
      subnets: [
        '123.223.47.0/24'
      ],
      uplinkPort: {
        device: 'string',
        port: 'string'
      },
      vlan: 100
    };
    describe('#addInternetNodeConnectionUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addInternetNodeConnectionUsingPOST('fakedata', syntheticDevicesAddInternetNodeConnectionUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('inet', data.response.name);
                assert.equal(true, Array.isArray(data.response.connections));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'addInternetNodeConnectionUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInternetNodeConnectionUsingDELETE - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteInternetNodeConnectionUsingDELETE('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'deleteInternetNodeConnectionUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntranetNodesUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIntranetNodesUsingGET('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.intranetNodes));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getIntranetNodesUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const syntheticDevicesPutIntranetNodesUsingPUTBodyParam = {
      intranetNodes: [
        {
          name: 'inet',
          connections: [
            {
              gatewayPort: {
                device: 'string',
                port: 'string'
              },
              peerIps: [
                '1.1.1.1'
              ],
              subnetAutoDiscovery: 'NONE',
              subnets: [
                '123.223.47.0/24'
              ],
              uplinkPort: {
                device: 'string',
                port: 'string'
              },
              vlan: 100
            }
          ]
        }
      ]
    };
    describe('#putIntranetNodesUsingPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putIntranetNodesUsingPUT('fakedata', syntheticDevicesPutIntranetNodesUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'putIntranetNodesUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntranetNodeUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIntranetNodeUsingGET('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('inet', data.response.name);
                assert.equal(true, Array.isArray(data.response.connections));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getIntranetNodeUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const syntheticDevicesPutIntranetNodeUsingPUTBodyParam = {
      name: 'inet',
      connections: [
        {
          gatewayPort: {
            device: 'string',
            port: 'string'
          },
          peerIps: [
            '1.1.1.1'
          ],
          subnetAutoDiscovery: 'NONE',
          subnets: [
            '123.223.47.0/24'
          ],
          uplinkPort: {
            device: 'string',
            port: 'string'
          },
          vlan: 100
        }
      ]
    };
    describe('#putIntranetNodeUsingPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putIntranetNodeUsingPUT('fakedata', 'fakedata', syntheticDevicesPutIntranetNodeUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'putIntranetNodeUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIntranetNodeUsingDELETE - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIntranetNodeUsingDELETE('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'deleteIntranetNodeUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const syntheticDevicesPatchIntranetNodeUsingPATCHBodyParam = {
      name: 'string'
    };
    describe('#patchIntranetNodeUsingPATCH - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchIntranetNodeUsingPATCH('fakedata', 'fakedata', syntheticDevicesPatchIntranetNodeUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'patchIntranetNodeUsingPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const syntheticDevicesAddIntranetNodeConnectionUsingPOSTBodyParam = {
      gatewayPort: {
        device: 'string',
        port: 'string'
      },
      peerIps: [
        '1.1.1.1'
      ],
      subnetAutoDiscovery: 'BGP_ROUTES',
      subnets: [
        '123.223.47.0/24'
      ],
      uplinkPort: {
        device: 'string',
        port: 'string'
      },
      vlan: 100
    };
    describe('#addIntranetNodeConnectionUsingPOST - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addIntranetNodeConnectionUsingPOST('fakedata', 'fakedata', syntheticDevicesAddIntranetNodeConnectionUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('inet', data.response.name);
                assert.equal(true, Array.isArray(data.response.connections));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'addIntranetNodeConnectionUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIntranetNodeConnectionUsingDELETE - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteIntranetNodeConnectionUsingDELETE('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'deleteIntranetNodeConnectionUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWanCircuitsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWanCircuitsUsingGET('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.wanCircuits));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getWanCircuitsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const syntheticDevicesPutWanCircuitsUsingPUTBodyParam = {
      wanCircuits: [
        {
          name: 'wan-circuit-01',
          connection1: {
            device: 'string',
            port: 'string',
            vlan: 100
          },
          connection2: {
            device: 'string',
            port: 'string',
            vlan: 100
          }
        }
      ]
    };
    describe('#putWanCircuitsUsingPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWanCircuitsUsingPUT('fakedata', syntheticDevicesPutWanCircuitsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'putWanCircuitsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWanCircuitUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWanCircuitUsingGET('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('wan-circuit-01', data.response.name);
                assert.equal('object', typeof data.response.connection1);
                assert.equal('object', typeof data.response.connection2);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getWanCircuitUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const syntheticDevicesPutWanCircuitUsingPUTBodyParam = {
      name: 'wan-circuit-01',
      connection1: {},
      connection2: {}
    };
    describe('#putWanCircuitUsingPUT - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWanCircuitUsingPUT('fakedata', 'fakedata', syntheticDevicesPutWanCircuitUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'putWanCircuitUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWanCircuitUsingDELETE - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWanCircuitUsingDELETE('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'deleteWanCircuitUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const syntheticDevicesPatchWanCircuitUsingPATCHBodyParam = {
      name: 'wan-circuit-01',
      connection1: {
        device: 'string',
        port: 'string',
        vlan: 100
      },
      connection2: {
        device: 'string',
        port: 'string',
        vlan: 100
      }
    };
    describe('#patchWanCircuitUsingPATCH - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchWanCircuitUsingPATCH('fakedata', 'fakedata', syntheticDevicesPatchWanCircuitUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'patchWanCircuitUsingPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCveIndexUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCveIndexUsingGET((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemAdministration', 'getCveIndexUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemAdministrationPutCveIndexUsingPUTBodyParam = {};
    describe('#putCveIndexUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putCveIndexUsingPUT(systemAdministrationPutCveIndexUsingPUTBodyParam, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemAdministration', 'putCveIndexUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportDatabaseUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.exportDatabaseUsingGET(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemAdministration', 'exportDatabaseUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importDatabaseUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.importDatabaseUsingPUT((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemAdministration', 'importDatabaseUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networksCreateWorkspaceNetworkUsingPOSTBodyParam = {
      name: 'My Network - Workspace A'
    };
    describe('#createWorkspaceNetworkUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createWorkspaceNetworkUsingPOST('fakedata', networksCreateWorkspaceNetworkUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'createWorkspaceNetworkUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAtlasUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAtlasUsingGET('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkLocations', 'getAtlasUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkLocationsUpdateAtlasUsingPATCHBodyParam = {};
    describe('#updateAtlasUsingPATCH - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateAtlasUsingPATCH('fakedata', networkLocationsUpdateAtlasUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkLocations', 'updateAtlasUsingPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLocationsUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLocationsUsingGET('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkLocations', 'getLocationsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkLocationsAddLocationUsingPOSTBodyParam = {
      name: 'Dayton DC',
      lat: 39.8113,
      lng: -84.2722
    };
    describe('#addLocationUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addLocationUsingPOST('fakedata', networkLocationsAddLocationUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkLocations', 'addLocationUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkLocationsPatchLocationsUsingPATCHBodyParam = [
      {}
    ];
    describe('#patchLocationsUsingPATCH - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchLocationsUsingPATCH('fakedata', networkLocationsPatchLocationsUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkLocations', 'patchLocationsUsingPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLocationUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getLocationUsingGET('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkLocations', 'getLocationUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLocationUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteLocationUsingDELETE('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkLocations', 'deleteLocationUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkLocationsPatchLocationUsingPATCHBodyParam = {
      name: 'Dayton DC',
      lat: 39.8113,
      lng: -84.2722,
      city: 'Dayton',
      adminDivision: 'Ohio',
      country: 'United States'
    };
    describe('#patchLocationUsingPATCH - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchLocationUsingPATCH('fakedata', 'fakedata', networkLocationsPatchLocationUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkLocations', 'patchLocationUsingPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    const putLocationsUsingPUTBodyParam = [
      {
        id: 'dyt-a',
        lat: 39.8113,
        lng: -84.2722,
        name: 'Dayton DC'
      }
    ];
    describe('#putLocationsUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putLocationsUsingPUT('fakedata', putLocationsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkLocations', 'putLocationsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceClustersUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceClustersUsingGET('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkLocations', 'getDeviceClustersUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkLocationsAddDeviceClusterUsingPOSTBodyParam = {
      name: 'atl-edge',
      devices: [
        'atl-edge-fw01',
        'atl-edge-fw02',
        'atl-isp-edge01',
        'atl-isp-edge02'
      ]
    };
    describe('#addDeviceClusterUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addDeviceClusterUsingPOST('fakedata', 'fakedata', networkLocationsAddDeviceClusterUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkLocations', 'addDeviceClusterUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkLocationsPutDeviceClustersUsingPUTBodyParam = [
      {}
    ];
    describe('#putDeviceClustersUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putDeviceClustersUsingPUT('fakedata', 'fakedata', networkLocationsPutDeviceClustersUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkLocations', 'putDeviceClustersUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceClusterUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeviceClusterUsingGET('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkLocations', 'getDeviceClusterUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDeviceClusterUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDeviceClusterUsingDELETE('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkLocations', 'deleteDeviceClusterUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkLocationsUpdateDeviceClusterUsingPATCHBodyParam = {
      name: 'atl-edge',
      devices: [
        'atl-edge-fw01',
        'atl-edge-fw02',
        'atl-isp-edge01',
        'atl-isp-edge02'
      ]
    };
    describe('#updateDeviceClusterUsingPATCH - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDeviceClusterUsingPATCH('fakedata', 'fakedata', 'fakedata', networkLocationsUpdateDeviceClusterUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkLocations', 'updateDeviceClusterUsingPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCollectionSchedulesUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCollectionSchedulesUsingGET('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkCollection', 'getCollectionSchedulesUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkCollectionAddCollectionScheduleUsingPOSTBodyParam = {
      enabled: true,
      timeZone: 'AMERICA_LOS_ANGELES',
      daysOfTheWeek: [
        0,
        2,
        4,
        6
      ]
    };
    describe('#addCollectionScheduleUsingPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addCollectionScheduleUsingPOST('fakedata', networkCollectionAddCollectionScheduleUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkCollection', 'addCollectionScheduleUsingPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCollectionScheduleUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getCollectionScheduleUsingGET('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkCollection', 'getCollectionScheduleUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkCollectionReplaceCollectionScheduleUsingPUTBodyParam = {
      id: 1,
      enabled: true,
      timeZone: 'AMERICA_LOS_ANGELES',
      daysOfTheWeek: [
        0,
        2,
        4,
        6
      ]
    };
    describe('#replaceCollectionScheduleUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.replaceCollectionScheduleUsingPUT('fakedata', 'fakedata', networkCollectionReplaceCollectionScheduleUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkCollection', 'replaceCollectionScheduleUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCollectionScheduleUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCollectionScheduleUsingDELETE('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkCollection', 'deleteCollectionScheduleUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceSourceUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeviceSourceUsingGET('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSetup', 'getDeviceSourceUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkSetupPatchDeviceSourceUsingPATCHBodyParam = {
      name: 'my_router',
      type: 'checkpoint_ssh',
      host: '10.121.7.13',
      port: 22,
      loginCredentialId: 'my-checkpoint-credentials',
      privilegedModePasswordId: samProps.authentication.password,
      shellCredentialId: 'my_avi_shell_cred',
      keyStoreId: 'my-tls-key-store',
      jumpServerId: 'my-sjc-jump-server',
      fullCollectionLog: true,
      disableIpv6Collection: false,
      collectBgpAdvertisements: true,
      bgpTableType: 'ADJ_RIB_IN',
      bgpPeerType: 'EBGP',
      bgpSubnetsToCollect: [
        '123.223.47.0/24'
      ],
      disableCollection: true,
      paginationMode: 'DISABLE_PAGINATION',
      largeRtt: true,
      openFlow: {
        port: 6653
      },
      note: 'Just for testing at the moment'
    };
    describe('#patchDeviceSourceUsingPATCH - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchDeviceSourceUsingPATCH('fakedata', 'fakedata', networkSetupPatchDeviceSourceUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkSetup', 'patchDeviceSourceUsingPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNqeQueriesUsingGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNqeQueriesUsingGET(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NQE', 'getNqeQueriesUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVulnerabilitiesUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getVulnerabilitiesUsingGET('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VulnerabilityAnalysis', 'getVulnerabilitiesUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEncryptorsUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getEncryptorsUsingGET('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getEncryptorsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const syntheticDevicesPutEncryptorsUsingPUTBodyParam = {
      encryptors: [
        {}
      ]
    };
    describe('#putEncryptorsUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putEncryptorsUsingPUT('fakedata', syntheticDevicesPutEncryptorsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'putEncryptorsUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEncryptorUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getEncryptorUsingGET('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getEncryptorUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const syntheticDevicesPutEncryptorUsingPUTBodyParam = {
      name: 'encryptor',
      siteConnection: {
        name: 'string'
      },
      underlayConnection: {
        name: 'string'
      },
      tunnels: [
        {}
      ]
    };
    describe('#putEncryptorUsingPUT - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putEncryptorUsingPUT('fakedata', 'fakedata', syntheticDevicesPutEncryptorUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'putEncryptorUsingPUT', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEncryptorUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteEncryptorUsingDELETE('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'deleteEncryptorUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const syntheticDevicesPatchEncryptorUsingPATCHBodyParam = {
      name: 'string',
      siteConnection: {
        uplinkPort: {
          device: 'string',
          port: 'string'
        },
        gatewayPort: {
          device: 'string',
          port: 'string'
        },
        vlan: 100,
        name: 'string'
      },
      underlayConnection: {
        uplinkPort: {
          device: 'string',
          port: 'string'
        },
        gatewayPort: {
          device: 'string',
          port: 'string'
        },
        vlan: 100,
        name: 'string'
      },
      tunnels: [
        {
          source: '1.1.1.1',
          destination: '2.2.2.2'
        }
      ],
      subnets: [
        'string'
      ]
    };
    describe('#patchEncryptorUsingPATCH - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchEncryptorUsingPATCH('fakedata', 'fakedata', syntheticDevicesPatchEncryptorUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'patchEncryptorUsingPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const syntheticDevicesUpdateInternetNodeUsingPATCHBodyParam = {
      connections: [
        {
          uplinkPort: {
            device: 'string',
            port: 'string'
          },
          gatewayPort: {
            device: 'string',
            port: 'string'
          },
          vlan: 100,
          name: 'string',
          site: 'string',
          subnets: [
            '123.223.47.0/24'
          ],
          subnetAutoDiscovery: 'NONE',
          peerIps: [
            '1.1.1.1'
          ],
          backdoorLinkPorts: [
            {
              device: 'string',
              port: 'string'
            }
          ],
          advertisesDefaultRoute: true
        }
      ],
      name: 'string'
    };
    describe('#updateInternetNodeUsingPATCH - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateInternetNodeUsingPATCH('fakedata', syntheticDevicesUpdateInternetNodeUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'updateInternetNodeUsingPATCH', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInternetNodeConnectionsUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getInternetNodeConnectionsUsingGET('fakedata', null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getInternetNodeConnectionsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInternetNodeConnectionsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteInternetNodeConnectionsUsingDELETE('fakedata', null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'deleteInternetNodeConnectionsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntranetNodeConnectionsUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getIntranetNodeConnectionsUsingGET('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getIntranetNodeConnectionsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIntranetNodeConnectionsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIntranetNodeConnectionsUsingDELETE('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'deleteIntranetNodeConnectionsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL2VpnConnectionsUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getL2VpnConnectionsUsingGET('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getL2VpnConnectionsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteL2VpnConnectionsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteL2VpnConnectionsUsingDELETE('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'deleteL2VpnConnectionsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL3VpnConnectionsUsingGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getL3VpnConnectionsUsingGET('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getL3VpnConnectionsUsingGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteL3VpnConnectionsUsingDELETE - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteL3VpnConnectionsUsingDELETE('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'deleteL3VpnConnectionsUsingDELETE', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInternetNodeUsingGETv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getInternetNodeUsingGETv1('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getInternetNodeUsingGETv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putInternetNodeUsingPUTv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putInternetNodeUsingPUTv1('fakedata', syntheticDevicesPutInternetNodeUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'putInternetNodeUsingPUTv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addInternetNodeConnectionUsingPOSTv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addInternetNodeConnectionUsingPOSTv1('fakedata', syntheticDevicesAddInternetNodeConnectionUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'addInternetNodeConnectionUsingPOSTv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntranetNodesUsingGETv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getIntranetNodesUsingGETv1('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getIntranetNodesUsingGETv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIntranetNodesUsingPUTv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putIntranetNodesUsingPUTv1('fakedata', syntheticDevicesPutIntranetNodesUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'putIntranetNodesUsingPUTv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIntranetNodeUsingGETv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getIntranetNodeUsingGETv1('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getIntranetNodeUsingGETv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putIntranetNodeUsingPUTv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putIntranetNodeUsingPUTv1('fakedata', 'fakedata', syntheticDevicesPutIntranetNodeUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'putIntranetNodeUsingPUTv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIntranetNodeUsingDELETEv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIntranetNodeUsingDELETEv1('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'deleteIntranetNodeUsingDELETEv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchIntranetNodeUsingPATCHv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchIntranetNodeUsingPATCHv1('fakedata', 'fakedata', syntheticDevicesPatchIntranetNodeUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'patchIntranetNodeUsingPATCHv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addIntranetNodeConnectionUsingPOSTv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addIntranetNodeConnectionUsingPOSTv1('fakedata', 'fakedata', syntheticDevicesAddIntranetNodeConnectionUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'addIntranetNodeConnectionUsingPOSTv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWanCircuitsUsingGETv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getWanCircuitsUsingGETv1('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getWanCircuitsUsingGETv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWanCircuitsUsingPUTv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWanCircuitsUsingPUTv1('fakedata', syntheticDevicesPutWanCircuitsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'putWanCircuitsUsingPUTv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWanCircuitUsingGETv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getWanCircuitUsingGETv1('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getWanCircuitUsingGETv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWanCircuitUsingPUTv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putWanCircuitUsingPUTv1('fakedata', 'fakedata', syntheticDevicesPutWanCircuitUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'putWanCircuitUsingPUTv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWanCircuitUsingDELETEv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteWanCircuitUsingDELETEv1('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'deleteWanCircuitUsingDELETEv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchWanCircuitUsingPATCHv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchWanCircuitUsingPATCHv1('fakedata', 'fakedata', syntheticDevicesPatchWanCircuitUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'patchWanCircuitUsingPATCHv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL2VpnsUsingGETv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getL2VpnsUsingGETv1('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getL2VpnsUsingGETv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putL2VpnsUsingPUTv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putL2VpnsUsingPUTv1('fakedata', syntheticDevicesPutL2VpnsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'putL2VpnsUsingPUTv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL2VpnUsingGETv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getL2VpnUsingGETv1('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getL2VpnUsingGETv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putL2VpnUsingPUTv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putL2VpnUsingPUTv1('fakedata', 'fakedata', syntheticDevicesPutL2VpnUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'putL2VpnUsingPUTv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteL2VpnUsingDELETEv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteL2VpnUsingDELETEv1('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'deleteL2VpnUsingDELETEv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchL2VpnUsingPATCHv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchL2VpnUsingPATCHv1('fakedata', 'fakedata', syntheticDevicesPatchL2VpnUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'patchL2VpnUsingPATCHv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addL2VpnConnectionUsingPOSTv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addL2VpnConnectionUsingPOSTv1('fakedata', 'fakedata', syntheticDevicesAddL2VpnConnectionUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'addL2VpnConnectionUsingPOSTv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL3VpnsUsingGETv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getL3VpnsUsingGETv1('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getL3VpnsUsingGETv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putL3VpnsUsingPUTv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putL3VpnsUsingPUTv1('fakedata', syntheticDevicesPutL3VpnsUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'putL3VpnsUsingPUTv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getL3VpnUsingGETv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getL3VpnUsingGETv1('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'getL3VpnUsingGETv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putL3VpnUsingPUTv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putL3VpnUsingPUTv1('fakedata', 'fakedata', syntheticDevicesPutL3VpnUsingPUTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'putL3VpnUsingPUTv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteL3VpnUsingDELETEv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteL3VpnUsingDELETEv1('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'deleteL3VpnUsingDELETEv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchL3VpnUsingPATCHv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchL3VpnUsingPATCHv1('fakedata', 'fakedata', syntheticDevicesPatchL3VpnUsingPATCHBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'patchL3VpnUsingPATCHv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addL3VpnConnectionUsingPOSTv1 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addL3VpnConnectionUsingPOSTv1('fakedata', 'fakedata', syntheticDevicesAddL3VpnConnectionUsingPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-forwardnetworks-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SyntheticDevices', 'addL3VpnConnectionUsingPOSTv1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
