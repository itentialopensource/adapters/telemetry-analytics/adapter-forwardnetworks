## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Forward Networks. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for ServiceNow.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the ForwardNetworks. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getNetworksUsingGETQuery(queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Lists all networks</td>
    <td style="padding:15px">{base_path}/{version}/api/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkUsingPOST(name, callback)</td>
    <td style="padding:15px">Creates a network</td>
    <td style="padding:15px">{base_path}/{version}/api/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkUsingDELETE(networkId, callback)</td>
    <td style="padding:15px">Deletes a network</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkUsingPATCH(networkId, update, callback)</td>
    <td style="padding:15px">Renames a network</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelCollectUsingPOST(networkId, force, callback)</td>
    <td style="padding:15px">Cancels an in-progress network collection</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/cancelcollection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCollectorStateUsingGETQuery(networkId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets the status of a network’s collector</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/collector/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">collectUsingPOST(networkId, request, callback)</td>
    <td style="padding:15px">Triggers a network collection</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/startcollection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceCredentialsUsingGETQuery(networkId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Lists a network’s device credentials</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/deviceCredentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceCredentialUsingPOST(networkId, deviceCredential, callback)</td>
    <td style="padding:15px">Creates or replaces a network device credential</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/deviceCredentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceCredentialUsingDELETE(networkId, credentialId, callback)</td>
    <td style="padding:15px">Deletes a network device credential</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/deviceCredentials/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDeviceCredentialUsingPATCH(networkId, credentialId, update, callback)</td>
    <td style="padding:15px">Updates a network device credential</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/deviceCredentials/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceSourcesUsingGETQuery(networkId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a network’s device sources</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/deviceSources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addOrUpdateDeviceSourcesUsingPOST(networkId, deviceSources, callback)</td>
    <td style="padding:15px">Creates or updates network device sources</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/deviceSources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceSourceUsingPUT(networkId, deviceSourceName, deviceSource, callback)</td>
    <td style="padding:15px">Creates or replaces a network device source</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/deviceSources/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceSourceUsingDELETEQuery(networkId, deviceSourceName, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Deletes a network device source</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/deviceSources/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJumpServersUsingGETQuery(networkId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Lists a network’s jump servers</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/jumpServers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createJumpServerUsingPOST(networkId, jumpServer, callback)</td>
    <td style="padding:15px">Creates or replaces a jump server</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/jumpServers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteJumpServerUsingDELETE(networkId, jumpServerId, callback)</td>
    <td style="padding:15px">Deletes a jump server</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/jumpServers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editJumpServerUsingPATCH(networkId, jumpServerId, update, callback)</td>
    <td style="padding:15px">Updates a jump server</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/jumpServers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkLayoutUsingGETQuery(networkId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets the network layout</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/layout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setNetworkLayoutUsingPOST(networkId, layout, callback)</td>
    <td style="padding:15px">Updates the network layout</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/layout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listNetworkSnapshotsUsingGETQuery(networkId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Lists all Snapshots</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importSnapshotWithFilesUsingPOST(file, networkId, note, callback)</td>
    <td style="padding:15px">Imports a Snapshot</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSnapshotUsingPOST(file, networkId, note, callback)</td>
    <td style="padding:15px">Imports a Snapshot</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLatestProcessedSnapshotUsingGETQuery(networkId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Returns the latest processed Snapshot</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/snapshots/latestProcessed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">zipSnapshotUsingGETQuery(snapshotId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Exports a Snapshot</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSnapshotUsingDELETE(snapshotId, callback)</td>
    <td style="padding:15px">Deletes a Snapshot</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSnapshotMetricsUsingGETQuery(snapshotId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Returns the metrics of a Snapshot</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setNetworkTopoListUsingPUT(networkId, links, callback)</td>
    <td style="padding:15px">Sets the topology overrides</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/topology?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTopologyUsingGETQuery(snapshotId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets the network topology</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/topology?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSnapshotTopoOverridesUsingGETQuery(snapshotId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets the topology overrides</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/topology/overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSnapshotTopoOverridesUsingPOST(snapshotId, linkOverridesEdit, callback)</td>
    <td style="padding:15px">Edits the topology overrides</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/topology/overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSnapshotTopoOverridesUsingPUT(snapshotId, linkOverrides, callback)</td>
    <td style="padding:15px">Sets the topology overrides</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/topology/overrides?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runNqeQueryUsingPOST(snapshotId, networkId, runRequest, callback)</td>
    <td style="padding:15px">Runs an NQE query on a Snapshot.</td>
    <td style="padding:15px">{base_path}/{version}/api/nqe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAvailablePredefinedChecksUsingGETQuery(queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets available Predefined checks</td>
    <td style="padding:15px">{base_path}/{version}/api/predefinedChecks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChecksUsingGETQuery(snapshotId, type = 'Isolation', queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets checks (with status)</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/checks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCheckUsingPOST(snapshotId, persistent, check, callback)</td>
    <td style="padding:15px">Adds a check</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/checks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateChecksUsingDELETE(snapshotId, callback)</td>
    <td style="padding:15px">Deactivates all checks</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/checks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSingleCheckUsingGETQuery(snapshotId, checkId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a check (with status)</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/checks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateCheckUsingDELETE(snapshotId, checkId, callback)</td>
    <td style="padding:15px">Deactivates a check</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/checks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAliasesUsingGETQuery(snapshotId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets all Aliases</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/aliases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSingleAliasUsingGETQuery(snapshotId, name, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets an Alias</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/aliases/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSnapshotAliasUsingPUT(snapshotId, name, aliasBuilder, callback)</td>
    <td style="padding:15px">Creates an Alias</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/aliases/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateAliasUsingDELETE(snapshotId, name, callback)</td>
    <td style="padding:15px">Deletes an Alias</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/aliases/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceFilesUsingGETQuery(snapshotId, deviceName, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Lists a device’s data files</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/devices/{pathv2}/files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceFileContentUsingGETQuery(snapshotId, deviceName, fileName, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets device data file content</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/devices/{pathv2}/files/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getL2VpnsUsingGETQuery(snapshotId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a network’s L2VPNs</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/l2Vpns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putL2VpnsUsingPUT(snapshotId, l2VpnList, callback)</td>
    <td style="padding:15px">Replaces all of a network’s L2VPNs</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/l2Vpns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getL2VpnUsingGETQuery(snapshotId, l2VpnName, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets an L2VPN</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/l2Vpns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putL2VpnUsingPUT(snapshotId, l2VpnName, l2Vpn, callback)</td>
    <td style="padding:15px">Adds or replaces an L2VPN</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/l2Vpns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteL2VpnUsingDELETE(snapshotId, l2VpnName, callback)</td>
    <td style="padding:15px">Removes an L2VPN</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/l2Vpns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchL2VpnUsingPATCH(snapshotId, l2VpnName, patch, callback)</td>
    <td style="padding:15px">Updates an L2VPN</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/l2Vpns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addL2VpnConnectionUsingPOST(snapshotId, l2VpnName, connection, callback)</td>
    <td style="padding:15px">Adds a connection to an L2VPN</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/l2Vpns/{pathv2}/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteL2VpnConnectionUsingDELETE(snapshotId, l2VpnName, deviceName, portName, callback)</td>
    <td style="padding:15px">Removes a connection from an L2VPN</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/l2Vpns/{pathv2}/connections/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getL3VpnsUsingGETQuery(snapshotId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a network’s L3VPNs</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/l3Vpns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putL3VpnsUsingPUT(snapshotId, l3Vpns, callback)</td>
    <td style="padding:15px">Replaces all of a network’s L3VPNs</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/l3Vpns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getL3VpnUsingGETQuery(snapshotId, l3VpnName, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets an L3VPN</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/l3Vpns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putL3VpnUsingPUT(snapshotId, l3VpnName, l3Vpn, callback)</td>
    <td style="padding:15px">Adds or replaces an L3VPN</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/l3Vpns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteL3VpnUsingDELETE(snapshotId, l3VpnName, callback)</td>
    <td style="padding:15px">Removes an L3VPN</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/l3Vpns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchL3VpnUsingPATCH(snapshotId, l3VpnName, patch, callback)</td>
    <td style="padding:15px">Updates an L3VPN</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/l3Vpns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addL3VpnConnectionUsingPOST(snapshotId, l3VpnName, connection, callback)</td>
    <td style="padding:15px">Adds a connection to an L3VPN</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/l3Vpns/{pathv2}/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteL3VpnEdgePortUsingDELETE(snapshotId, l3VpnName, deviceName, portName, callback)</td>
    <td style="padding:15px">Removes a connection from an L3VPN</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/l3Vpns/{pathv2}/connections/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPathsBySnapshotUsingGET(snapshotId, srcIp, dstIp, intent = 'PREFER_VIOLATIONS', ipProto, srcPort, dstPort, icmpType, includeNetworkFunctions, maxCandidates, maxResults, maxReturnPathResults, maxSeconds, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Searches for paths by tracing packets through the network</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/paths?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPathsByNetworkIdUsingGET(networkId, srcIp, dstIp, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Searches for paths by tracing packets through the network</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/paths?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPathsBulkUsingPOST(snapshotId, request, callback)</td>
    <td style="padding:15px">Searches for paths by tracing sets of packets through the network</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/pathsBulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTracePathsBulkSeqUsingPOST(snapshotId, request, callback)</td>
    <td style="padding:15px">Searches for paths by tracing sets of packets through the network</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/pathsBulkSeq?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPathsBulkByNetworkIdUsingPOST(networkId, snapshotId, request, callback)</td>
    <td style="padding:15px">Searches for paths by tracing sets of packets through the network</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/paths-bulk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTracePathsBulkSeqByNetworkIdUsingPOST(networkId, snapshotId, request, callback)</td>
    <td style="padding:15px">Searches for paths by tracing sets of packets through the network</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/paths-bulk-seq?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApiVersionUsingGETQuery(queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets the API’s current version</td>
    <td style="padding:15px">{base_path}/{version}/api/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDeviceCredentialsUsingPATCH(networkId, credentials, callback)</td>
    <td style="padding:15px">Creates or replaces network device credentials</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/deviceCredentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceSourcesUsingDELETE(networkId, criteria, callback)</td>
    <td style="padding:15px">Deletes network device sources</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/deviceSources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">customZipSnapshotUsingPOST(snapshotId, params, callback)</td>
    <td style="padding:15px">Exports a Snapshot subset, optionally obfuscated</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateAliasesUsingDELETE(snapshotId, name, callback)</td>
    <td style="padding:15px">Deletes Aliases</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/aliases?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesUsingGETQuery(snapshotId, name, displayName, sourceName, type, manufacturer, model, platform, osVersion, collectionError, processingError, skip, limit, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a network’s devices</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesByNetworkIdUsingGET(networkId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a network’s devices</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOneDeviceUsingGETQuery(snapshotId, deviceName, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a network device</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMissingDevicesUsingGETQuery(snapshotId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a network’s missing devices</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/missingDevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInternetNodeUsingGETQuery(snapshotId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets the network’s internet node</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/internetNode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putInternetNodeUsingPUT(snapshotId, internetNode, callback)</td>
    <td style="padding:15px">Creates or replaces the network's internet node</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/internetNode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInternetNodeUsingDELETE(snapshotId, callback)</td>
    <td style="padding:15px">Removes the internet node</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/internetNode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addInternetNodeConnectionUsingPOST(snapshotId, connection, callback)</td>
    <td style="padding:15px">Adds a connection to the internet node</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/internetNode/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInternetNodeConnectionUsingDELETE(snapshotId, deviceName, portName, callback)</td>
    <td style="padding:15px">Removes a connection from the internet node</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/internetNode/connections/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntranetNodesUsingGETQuery(snapshotId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a network’s intranet nodes</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/intranetNodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIntranetNodesUsingPUT(snapshotId, intranetNodeList, callback)</td>
    <td style="padding:15px">Replaces all of a network’s intranet nodes</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/intranetNodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntranetNodeUsingGETQuery(snapshotId, nodeName, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets an intranet node</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/intranetNodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIntranetNodeUsingPUT(snapshotId, nodeName, intranetNode, callback)</td>
    <td style="padding:15px">Adds or replaces an intranet node</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/intranetNodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIntranetNodeUsingDELETE(snapshotId, nodeName, callback)</td>
    <td style="padding:15px">Removes an intranet node</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/intranetNodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIntranetNodeUsingPATCH(snapshotId, nodeName, patch, callback)</td>
    <td style="padding:15px">Updates an intranet node</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/intranetNodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIntranetNodeConnectionUsingPOST(snapshotId, nodeName, connection, callback)</td>
    <td style="padding:15px">Adds a connection to an intranet node</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/intranetNodes/{pathv2}/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIntranetNodeConnectionUsingDELETE(snapshotId, nodeName, deviceName, portName, callback)</td>
    <td style="padding:15px">Removes a connection from an intranet node</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/intranetNodes/{pathv2}/connections/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWanCircuitsUsingGETQuery(snapshotId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a network’s WAN circuits</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/wanCircuits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWanCircuitsUsingPUT(snapshotId, wanCircuitList, callback)</td>
    <td style="padding:15px">Replaces all of a network’s WAN circuits</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/wanCircuits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWanCircuitUsingGETQuery(snapshotId, wanCircuitName, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a WAN circuit</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/wanCircuits/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWanCircuitUsingPUT(snapshotId, wanCircuitName, wanCircuit, callback)</td>
    <td style="padding:15px">Adds or replaces a WAN circuit</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/wanCircuits/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWanCircuitUsingDELETE(snapshotId, wanCircuitName, callback)</td>
    <td style="padding:15px">Removes a WAN circuit</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/wanCircuits/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchWanCircuitUsingPATCH(snapshotId, wanCircuitName, patch, callback)</td>
    <td style="padding:15px">Updates a WAN circuit</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/wanCircuits/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCveIndexUsingGETQuery(queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Exports the CVE index</td>
    <td style="padding:15px">{base_path}/{version}/api/cve-index?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCveIndexUsingPUT(content, sha, callback)</td>
    <td style="padding:15px">Imports a new CVE index</td>
    <td style="padding:15px">{base_path}/{version}/api/cve-index?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportDatabaseUsingGETQuery(variant, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Exports a database backup .zip file</td>
    <td style="padding:15px">{base_path}/{version}/api/database?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importDatabaseUsingPUT(callback)</td>
    <td style="padding:15px">Restores from a database backup .zip file</td>
    <td style="padding:15px">{base_path}/{version}/api/database?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createWorkspaceNetworkUsingPOST(networkId, request, callback)</td>
    <td style="padding:15px">Creates a Workspace network</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/workspaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAtlasUsingGETQuery(networkId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets device locations</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/atlas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAtlasUsingPATCH(networkId, patch, callback)</td>
    <td style="padding:15px">Updates device locations</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/atlas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLocationsUsingGETQuery(networkId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a network’s locations</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/locations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addLocationUsingPOST(networkId, newLocation, callback)</td>
    <td style="padding:15px">Creates a network location</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/locations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchLocationsUsingPATCH(networkId, patches, callback)</td>
    <td style="padding:15px">Creates or updates network locations</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/locations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLocationsUsingPUT(networkId, locations, callback)</td>
    <td style="padding:15px">Creates network locations</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/locations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLocationUsingGETQuery(networkId, locationId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a network location</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/locations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLocationUsingDELETE(networkId, locationId, callback)</td>
    <td style="padding:15px">Deletes a network location</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/locations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchLocationUsingPATCH(networkId, locationId, patch, callback)</td>
    <td style="padding:15px">Updates a network location</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/locations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceClustersUsingGETQuery(networkId, locationId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets the device clusters at a location</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/locations/{pathv2}/clusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDeviceClusterUsingPOST(networkId, locationId, cluster, callback)</td>
    <td style="padding:15px">Creates a device cluster at a location</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/locations/{pathv2}/clusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDeviceClustersUsingPUT(networkId, locationId, clusters, callback)</td>
    <td style="padding:15px">Replaces all device clusters at a location</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/locations/{pathv2}/clusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceClusterUsingGETQuery(networkId, locationId, clusterName, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a device cluster</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/locations/{pathv2}/clusters/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceClusterUsingDELETE(networkId, locationId, clusterName, callback)</td>
    <td style="padding:15px">Deletes a device cluster</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/locations/{pathv2}/clusters/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceClusterUsingPATCH(networkId, locationId, clusterName, update, callback)</td>
    <td style="padding:15px">Updates a device cluster</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/locations/{pathv2}/clusters/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCollectionSchedulesUsingGETQuery(networkId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a network’s collection schedules</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/collection-schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCollectionScheduleUsingPOST(networkId, schedule, callback)</td>
    <td style="padding:15px">Adds a network collection schedule</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/collection-schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCollectionScheduleUsingGETQuery(networkId, scheduleId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a network collection schedule</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/collection-schedules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceCollectionScheduleUsingPUT(networkId, scheduleId, schedule, callback)</td>
    <td style="padding:15px">Replaces a network collection schedule</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/collection-schedules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCollectionScheduleUsingDELETE(networkId, scheduleId, callback)</td>
    <td style="padding:15px">Deletes a network collection schedule</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/collection-schedules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceSourceUsingGETQuery(networkId, deviceSourceName, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a network device source</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/deviceSources/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDeviceSourceUsingPATCH(networkId, deviceSourceName, patch, callback)</td>
    <td style="padding:15px">Updates a network device source</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/deviceSources/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNqeQueriesUsingGETQuery(dir, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Lists all NQE queries</td>
    <td style="padding:15px">{base_path}/{version}/api/nqe/queries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVulnerabilitiesUsingGETQuery(snapshotId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets vulnerabilities and possibly impacted devices</td>
    <td style="padding:15px">{base_path}/{version}/api/snapshots/{pathv1}/vulnerabilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEncryptorsUsingGETQuery(networkId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a network's encryptors</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/encryptors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEncryptorsUsingPUT(networkId, encryptorList, callback)</td>
    <td style="padding:15px">Replaces all of a network’s encryptors</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/encryptors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEncryptorUsingGETQuery(networkId, deviceName, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets an encryptor</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/encryptors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putEncryptorUsingPUT(networkId, deviceName, encryptor, callback)</td>
    <td style="padding:15px">Adds or replaces an encryptor</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/encryptors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEncryptorUsingDELETE(networkId, deviceName, callback)</td>
    <td style="padding:15px">Removes an encryptor</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/encryptors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchEncryptorUsingPATCH(networkId, deviceName, patch, callback)</td>
    <td style="padding:15px">Updates an encryptor</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/encryptors/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateInternetNodeUsingPATCH(networkId, patch, callback)</td>
    <td style="padding:15px">Updates the network’s internet node</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/internet-node?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInternetNodeConnectionsUsingGETQuery(networkId, uplinkDevice, uplinkPort, gatewayDevice, gatewayPort, vlan, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets some internet node connections</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/internet-node/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInternetNodeConnectionsUsingDELETE(networkId, uplinkDevice, uplinkPort, gatewayDevice, gatewayPort, vlan, callback)</td>
    <td style="padding:15px">Removes internet node connections</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/internet-node/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntranetNodeConnectionsUsingGETQuery(networkId, nodeName, uplinkDevice, uplinkPort, gatewayDevice, gatewayPort, vlan, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets an intranet node’s connections</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/intranet-nodes/{pathv2}/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIntranetNodeConnectionsUsingDELETE(networkId, nodeName, uplinkDevice, uplinkPort, gatewayDevice, gatewayPort, vlan, callback)</td>
    <td style="padding:15px">Removes an intranet node’s connections</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/intranet-nodes/{pathv2}/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getL2VpnConnectionsUsingGETQuery(networkId, l2VpnName, device, port, vlan, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets an L2VPN’s connections</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/l2-vpns/{pathv2}/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteL2VpnConnectionsUsingDELETE(networkId, l2VpnName, device, port, vlan, callback)</td>
    <td style="padding:15px">Removes an L2VPN’s connections</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/l2-vpns/{pathv2}/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getL2VpnsUsingGETv1Query(networkId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a network’s L2VPNs</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/l2-vpns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putL2VpnsUsingPUTv1(networkId, l2VpnList, callback)</td>
    <td style="padding:15px">Replaces all of a network’s L2VPNs</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/l2-vpns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getL2VpnUsingGETv1Query(networkId, l2VpnName, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets an L2VPN</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/l2-vpns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putL2VpnUsingPUTv1(networkId, l2VpnName, l2Vpn, callback)</td>
    <td style="padding:15px">Adds or replaces an L2VPN</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/l2-vpns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteL2VpnUsingDELETEv1(networkId, l2VpnName, callback)</td>
    <td style="padding:15px">Removes an L2VPN</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/l2-vpns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchL2VpnUsingPATCHv1(networkId, l2VpnName, patch, callback)</td>
    <td style="padding:15px">Updates an L2VPN</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/l2-vpns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addL2VpnConnectionUsingPOSTv1(networkId, l2VpnName, connection, callback)</td>
    <td style="padding:15px">Adds a connection to an L2VPN</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/l2-vpns/{pathv2}/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getL3VpnConnectionsUsingGETQuery(networkId, l3VpnName, uplinkDevice, uplinkPort, gatewayDevice, gatewayPort, vlan, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets an L3VPN’s connections</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/l3-vpns/{pathv2}/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteL3VpnConnectionsUsingDELETE(networkId, l3VpnName, uplinkDevice, uplinkPort, gatewayDevice, gatewayPort, vlan, callback)</td>
    <td style="padding:15px">Removes an L3VPN’s connections</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/l3-vpns/{pathv2}/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getL3VpnsUsingGETv1Query(networkId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a network’s L3VPNs</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/l3-vpns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putL3VpnsUsingPUTv1(networkId, l3VpnList, callback)</td>
    <td style="padding:15px">Replaces all of a network’s L3VPNs</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/l3-vpns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getL3VpnUsingGETv1Query(networkId, l3VpnName, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets an L3VPN</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/l3-vpns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putL3VpnUsingPUTv1(networkId, l3VpnName, l3Vpn, callback)</td>
    <td style="padding:15px">Adds or replaces an L3VPN</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/l3-vpns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteL3VpnUsingDELETEv1(networkId, l3VpnName, callback)</td>
    <td style="padding:15px">Removes an L3VPN</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/l3-vpns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchL3VpnUsingPATCHv1(networkId, l3VpnName, patch, callback)</td>
    <td style="padding:15px">Updates an L3VPN</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/l3-vpns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addL3VpnConnectionUsingPOSTv1(networkId, l3VpnName, connection, callback)</td>
    <td style="padding:15px">Adds a connection to an L3VPN</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/l3-vpns/{pathv2}/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInternetNodeUsingGETv1Query(networkId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets the network’s internet node</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/internet-node?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putInternetNodeUsingPUTv1(networkId, internetNode, callback)</td>
    <td style="padding:15px">Replaces the network’s internet node</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/internet-node?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addInternetNodeConnectionUsingPOSTv1(networkId, connection, callback)</td>
    <td style="padding:15px">Adds a connection to the internet node</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/internet-node/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntranetNodesUsingGETv1Query(networkId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a network’s intranet nodes</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/intranet-nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIntranetNodesUsingPUTv1(networkId, intranetNodeList, callback)</td>
    <td style="padding:15px">Replaces all of a network’s intranet nodes</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/intranet-nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntranetNodeUsingGETv1Query(networkId, nodeName, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets an intranet node</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/intranet-nodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIntranetNodeUsingPUTv1(networkId, nodeName, intranetNode, callback)</td>
    <td style="padding:15px">Adds or replaces an intranet node</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/intranet-nodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIntranetNodeUsingDELETEv1(networkId, nodeName, callback)</td>
    <td style="padding:15px">Removes an intranet node</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/intranet-nodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIntranetNodeUsingPATCHv1(networkId, nodeName, patch, callback)</td>
    <td style="padding:15px">Updates an intranet node</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/intranet-nodes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIntranetNodeConnectionUsingPOSTv1(networkId, nodeName, connection, callback)</td>
    <td style="padding:15px">Adds a connection to an intranet node</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/intranet-nodes/{pathv2}/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWanCircuitsUsingGETv1Query(networkId, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a network’s WAN circuits</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/wan-circuits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWanCircuitsUsingPUTv1(networkId, wanCircuitList, callback)</td>
    <td style="padding:15px">Replaces all of a network’s WAN circuits</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/wan-circuits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWanCircuitUsingGETv1Query(networkId, wanCircuitName, queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Gets a WAN circuit</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/wan-circuits/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWanCircuitUsingPUTv1(networkId, wanCircuitName, wanCircuit, callback)</td>
    <td style="padding:15px">Adds or replaces a WAN circuit</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/wan-circuits/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWanCircuitUsingDELETEv1(networkId, wanCircuitName, callback)</td>
    <td style="padding:15px">Removes a WAN circuit</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/wan-circuits/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchWanCircuitUsingPATCHv1(networkId, wanCircuitName, patch, callback)</td>
    <td style="padding:15px">Updates a WAN circuit</td>
    <td style="padding:15px">{base_path}/{version}/api/networks/{pathv1}/wan-circuits/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getL7ApplicationsUsingGET(queryObject, additionalInfo, callback)</td>
    <td style="padding:15px">Lists known L7 applications</td>
    <td style="padding:15px">{base_path}/{version}/api/l7-applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
